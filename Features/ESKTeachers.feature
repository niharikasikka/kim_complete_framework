﻿Feature: ESkTeacher
	Add a teacher to a service or to multiple services
	Add multiple teachers to a service or multiple services
	Edit an existing teacher to a service
	Remove an existing teacher from a service

	# The scenarios below are for ESK teachers only so please change the dob, vit number if it needs to be
	# Change the name of teachers, vit number, dob and address to create unique enrolments
	# Child commence date can be updated depending on the current date and month
	# Delete teacher: Please mention an existing teacher

@teacher
Scenario Outline: AddESkTeacher
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add ESK teachers
	| Given Name | Family Name | Gender | Date of Birth  | CommencementDate | RegistrationStatus       | VITNumber   | University                    | Course                                             | Year |                         
	| Martin     | Hall        | Male   | " 03/05/1992 " | " 02/04/2016 "   | Fully registered teacher | 406717    | Australian Catholic University  | Bachelor of Education (Early Childhood and Primary)| 2016 |   
	And I close the browser successfully
	Examples:
	| service    |
	| Happy Kids |
	
	@teacher
Scenario Outline: AddMultipleESKTeachers
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add ESK teachers
	| Given Name | Family Name | Gender | Date of Birth  | CommencementDate | RegistrationStatus       | VITNumber | University      | Course                          | Year     |
	| John1      | Watson      | Male   | " 11/09/1984 " | " 23/02/2019 "   | Fully registered teacher | 400108    | Bond University | Bachelor of Children's Services | " 2015 " |
	| Roop1      | Jain        | Female | " 21/01/1993 " | " 01/07/2018 "   | Fully registered teacher | 400105    | Bond University | Bachelor of Children's Services | " 2017 " |    
	And I close the browser successfully
	Examples:
	| service       |
	| Happy Kids    |

	@teacher
Scenario Outline: EditESKTeacher
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I open an existing ESKteacher with <name>
	And I edit <commencedate> of ESKteacher
	And I edit the <yearawarded> 
	And I close the browser successfully
	Examples:
	| service        | name                  | commencedate   | yearawarded   |
	| Happy Kids     | Martin Hall           | " 04/01/2017 " |  " 2017 "     |

@teacher
Scenario Outline: DeleteESKTeacher
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I delete an existing ESKteacher with <name>
	And I enter lastdate and <reason> for leaving and confirm removal
	And I verify that ESKteacher with <name> is deleted successfully 
	And I close the browser successfully
	Examples: 
	| service                      | name                 | reason   |
	| Laverton Child Kindergarten  | Chris Martin         | Resigned |
##
## BELOW SCENARIO IS TO TEST SHOW INACTIVE "ESK" TEACHER
## IN TABLE BELOW, INPUT THE DETAILS FOR THE TEACHERS THAT WILL BE ADDED
## IN SERVICE SECTION BELOW (EXAMPLES), INPUT THE RELATED SERVICE WHERE YOU WISH TO HAVE TEACHER BE ADDED
@teacher
Scenario Outline: ShowInactiveMakeitActiveESKTeacher
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add click show inactive and make inactive ESK teacher	to active
	And I close the browser successfully
	Examples:
	| service    |
	| The Grange Kindergarten |