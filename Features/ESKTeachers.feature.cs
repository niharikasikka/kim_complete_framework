// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:3.0.0.0
//      SpecFlow Generator Version:3.0.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace KIM_Automation.Features
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.0.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [TechTalk.SpecRun.FeatureAttribute("ESkTeacher", Description="\tAdd a teacher to a service or to multiple services\r\n\tAdd multiple teachers to a " +
        "service or multiple services\r\n\tEdit an existing teacher to a service\r\n\tRemove an" +
        " existing teacher from a service", SourceFile="Features\\ESKTeachers.feature", SourceLine=0)]
    public partial class ESkTeacherFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "ESKTeachers.feature"
#line hidden
        
        [TechTalk.SpecRun.FeatureInitialize()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "ESkTeacher", "\tAdd a teacher to a service or to multiple services\r\n\tAdd multiple teachers to a " +
                    "service or multiple services\r\n\tEdit an existing teacher to a service\r\n\tRemove an" +
                    " existing teacher from a service", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [TechTalk.SpecRun.FeatureCleanup()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        public virtual void TestInitialize()
        {
        }
        
        [TechTalk.SpecRun.ScenarioCleanup()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void AddESkTeacher(string service, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "teacher"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("AddESkTeacher", null, @__tags);
#line 13
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 14
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 15
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 16
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 17
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 18
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 19
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 20
 testRunner.Then(string.Format("I verify the {0} is added successfully to the organisation in Kim sharepoint", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            TechTalk.SpecFlow.Table table16 = new TechTalk.SpecFlow.Table(new string[] {
                        "Given Name",
                        "Family Name",
                        "Gender",
                        "Date of Birth",
                        "CommencementDate",
                        "RegistrationStatus",
                        "VITNumber",
                        "University",
                        "Course",
                        "Year"});
            table16.AddRow(new string[] {
                        "Martin",
                        "Hall",
                        "Male",
                        "\" 03/05/1992 \"",
                        "\" 02/04/2016 \"",
                        "Fully registered teacher",
                        "406717",
                        "Australian Catholic University",
                        "Bachelor of Education (Early Childhood and Primary)",
                        "2016"});
#line 21
 testRunner.And("I add ESK teachers", ((string)(null)), table16, "And ");
#line 24
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("AddESkTeacher, Happy Kids", new string[] {
                "teacher"}, SourceLine=26)]
        public virtual void AddESkTeacher_HappyKids()
        {
#line 13
this.AddESkTeacher("Happy Kids", ((string[])(null)));
#line hidden
        }
        
        public virtual void AddMultipleESKTeachers(string service, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "teacher"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("AddMultipleESKTeachers", null, @__tags);
#line 30
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 31
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 32
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 33
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 34
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 35
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 36
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 37
 testRunner.Then(string.Format("I verify the {0} is added successfully to the organisation in Kim sharepoint", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            TechTalk.SpecFlow.Table table17 = new TechTalk.SpecFlow.Table(new string[] {
                        "Given Name",
                        "Family Name",
                        "Gender",
                        "Date of Birth",
                        "CommencementDate",
                        "RegistrationStatus",
                        "VITNumber",
                        "University",
                        "Course",
                        "Year"});
            table17.AddRow(new string[] {
                        "John1",
                        "Watson",
                        "Male",
                        "\" 11/09/1984 \"",
                        "\" 23/02/2019 \"",
                        "Fully registered teacher",
                        "400108",
                        "Bond University",
                        "Bachelor of Children\'s Services",
                        "\" 2015 \""});
            table17.AddRow(new string[] {
                        "Roop1",
                        "Jain",
                        "Female",
                        "\" 21/01/1993 \"",
                        "\" 01/07/2018 \"",
                        "Fully registered teacher",
                        "400105",
                        "Bond University",
                        "Bachelor of Children\'s Services",
                        "\" 2017 \""});
#line 38
 testRunner.And("I add ESK teachers", ((string)(null)), table17, "And ");
#line 42
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("AddMultipleESKTeachers, Happy Kids", new string[] {
                "teacher"}, SourceLine=44)]
        public virtual void AddMultipleESKTeachers_HappyKids()
        {
#line 30
this.AddMultipleESKTeachers("Happy Kids", ((string[])(null)));
#line hidden
        }
        
        public virtual void EditESKTeacher(string service, string name, string commencedate, string yearawarded, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "teacher"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("EditESKTeacher", null, @__tags);
#line 48
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 49
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 50
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 51
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 52
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 53
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 54
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 55
 testRunner.Then(string.Format("I verify the {0} is added successfully to the organisation in Kim sharepoint", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 56
 testRunner.And(string.Format("I open an existing ESKteacher with {0}", name), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 57
 testRunner.And(string.Format("I edit {0} of ESKteacher", commencedate), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 58
 testRunner.And(string.Format("I edit the {0}", yearawarded), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 59
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("EditESKTeacher, Happy Kids", new string[] {
                "teacher"}, SourceLine=61)]
        public virtual void EditESKTeacher_HappyKids()
        {
#line 48
this.EditESKTeacher("Happy Kids", "Martin Hall", "\" 04/01/2017 \"", "\" 2017 \"", ((string[])(null)));
#line hidden
        }
        
        public virtual void DeleteESKTeacher(string service, string name, string reason, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "teacher"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("DeleteESKTeacher", null, @__tags);
#line 65
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 66
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 67
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 68
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 69
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 70
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 71
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 72
 testRunner.Then(string.Format("I verify the {0} is added successfully to the organisation in Kim sharepoint", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 73
 testRunner.And(string.Format("I delete an existing ESKteacher with {0}", name), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 74
 testRunner.And(string.Format("I enter lastdate and {0} for leaving and confirm removal", reason), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 75
 testRunner.And(string.Format("I verify that ESKteacher with {0} is deleted successfully", name), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 76
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("DeleteESKTeacher, Laverton Child Kindergarten", new string[] {
                "teacher"}, SourceLine=78)]
        public virtual void DeleteESKTeacher_LavertonChildKindergarten()
        {
#line 65
this.DeleteESKTeacher("Laverton Child Kindergarten", "Chris Martin", "Resigned", ((string[])(null)));
#line hidden
        }
        
        public virtual void ShowInactiveMakeitActiveESKTeacher(string service, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "teacher"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("ShowInactiveMakeitActiveESKTeacher", null, @__tags);
#line 85
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 86
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 87
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 88
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 89
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 90
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 91
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 92
 testRunner.Then(string.Format("I verify the {0} is added successfully to the organisation in Kim sharepoint", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 93
 testRunner.And("I add click show inactive and make inactive ESK teacher\tto active", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 94
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("ShowInactiveMakeitActiveESKTeacher, The Grange Kindergarten", new string[] {
                "teacher"}, SourceLine=96)]
        public virtual void ShowInactiveMakeitActiveESKTeacher_TheGrangeKindergarten()
        {
#line 85
this.ShowInactiveMakeitActiveESKTeacher("The Grange Kindergarten", ((string[])(null)));
#line hidden
        }
        
        [TechTalk.SpecRun.TestRunCleanup()]
        public virtual void TestRunCleanup()
        {
            TechTalk.SpecFlow.TestRunnerManager.GetTestRunner().OnTestRunEnd();
        }
    }
}
#pragma warning restore
#endregion
