﻿Feature: Service
	Login to KIM1 and adding a new service to an organisation
	Login to KIM1 and adding multiple services to an organisation
	Edit an existing service
	Remove an existing service
##############################################################################################################
##
## This feature file covers the following:
## Add Multiple Services to an organization/SP/service provider which is an EYM organization
## Add Service to an organization/SP
## Add Service EYM to an organization/SP/service provider which is an EYM organization
## Apply EYM for existing service
## Delete an existing service
## Edit an existing service
## Recommence Service (recommence service with is not yet funded)
########## databse query - 
########## select o.Name, s.ServiceID, s.Name, s.isFunded, o.IsCluster from dbo.tblOrganisation as o
########## inner join tblservice as s on o.OrganisationID = s.OrganisationID
########## where s.isFunded = 0 and o.IsCluster = 0)
## 
## Recommence Service from EYM SP as EYM but not submit the application 
## (recommence service with is not yet funded and under service provider which is an EYM organization)
########## databse query - 
########## select o.Name, s.ServiceID, s.Name, s.isFunded, o.IsCluster from dbo.tblOrganisation as o
########## inner join tblservice as s on o.OrganisationID = s.OrganisationID
########## where s.isFunded = 0 and o.IsCluster = 1 )
## 
## Recommence Service from EYM SP as EYM and submit the application
## (recommence service with is not yet funded and under service provider which is an EYM organization)
########## databse query - 
########## select o.Name, s.ServiceID, s.Name, s.isFunded, o.IsCluster from dbo.tblOrganisation as o
########## inner join tblservice as s on o.OrganisationID = s.OrganisationID
########## where s.isFunded = 0 and o.IsCluster = 1 )
## 
## Recommence Service from EYM SP as not EYM
## (recommence service with is not yet funded and under service provider which is an EYM organization)
########## databse query - 
########## select o.Name, s.ServiceID, s.Name, s.isFunded, o.IsCluster from dbo.tblOrganisation as o
########## inner join tblservice as s on o.OrganisationID = s.OrganisationID
########## where s.isFunded = 0 and o.IsCluster = 1 )
## 
## Transfer existing service
## <may add some more from here as needed>
##
## DON'T FORGET TO UPDATE THE SERVICEPROVIDER in KIM_SmokeTest>Data>Resources.resx.
## NOTE THAT SERVICE NEED TO BE IDENTIFIED IN THE TABLE BELOW (EXAMPLES: SECTION)
## ENSURE TO BUILD/REBUILD EVERYTIME THERE'S CHANGES IN THIS FILE
##
##############################################################################################################
####
## BELOW SCENARIO IS TO TEST ADDING A SERVICE
## IN SERVICE SECTION BELOW (EXAMPLES), INPUT THE RELATED SERVICE WHERE YOU WISH TO TEST THE ADJUSTMENTS TAB
@service
Scenario: AddService
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	And I add a new service and fill in details and submit	
	| Service Name			| Date Commence | Unit Number | Street         | Street Type | Suburb   | State | Postcode | LGA Code         | Telephone  | Email                            | Title Emergency | Given Name Emergency | Family Name Emergency | Email Emergency           | Position Emergency | Telephone Emergency | Mobile Emergency | Quality Rating |
	| NCL Eym_No Service	| 17/06/2019    | 2           | Tassell Street | ALLEY       | Hadfield | VIC   | 3234     | Alpine (S) 20110 | 0469347455 | sikka.niharka@edumail.vic.gov.au | Mr              | Will                 | Smith                 | williamsmith222@gmail.com | System             | 0469939394          | 0465547409       | Excellent      |
	Then I verify that the service is added successfully
	And I close the browser successfully

	# This script adds a new service in Kim sharepoint
	# The service type is EYM and details related to EYM service is also filled
	# To create unique services change the service name, postcode and street name
	# Make sure date commence is appropriate
@service
Scenario: AddService_EYM
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	And I add a new EYM service and fill in details and submit	
	| Service Name		| Date Commence | Unit Number | Street         | Street Type | Suburb   | State | Postcode | LGA Code         | Telephone  | Email                            | Title Emergency | Given Name Emergency | Family Name Emergency | Email Emergency           | Position Emergency | Telephone Emergency | Mobile Emergency | Quality Rating |
	| NCL Eym3 Service	| 17/06/2019    | 2A          | Tassell Street | ALLEY       | Hadfield | VIC   | 3144     | Alpine (S) 20110 | 0469347455 | sikka.niharka@edumail.vic.gov.au | Mr              | Will                 | Smith                 | williamsmith222@gmail.com | System             | 0469939394          | 0465547409       | Excellent      |
	Then I verify that the service is added successfully
	And I close the browser successfully

@service
Scenario: AddMultipleServices
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	And I add multiple services and fill in details and submit	
	| Service Name							| Date Commence | Unit Number | Street          | Street Type | Suburb      | State | Postcode | LGA Code         | Telephone  | Email                             | Title Emergency | Given Name Emergency | Family Name Emergency   | Email Emergency           | Position Emergency | Telephone Emergency | Mobile Emergency | Quality Rating |
	| Community Early-years Childcare - NC1	| 10/07/2019    | 222          | Hoddy Street1  | ALLEY       | Monee Ponds | VIC   | 3141     | Alpine (S) 20110 | 0469347455 | sikka.niharika@edumail.vic.gov.au | Mr              | William              | Smith                   | williamsmith2222@gmail.com| System             | 0469939394          | 0465547402       | Excellent      |
	| Community Early-years Childcare - NC2	| 10/07/2019    | 222          | Hoddy Street2  | ALLEY       | Monee Ponds | VIC   | 3141     | Alpine (S) 20110 | 0469347455 | sikka.niharika@edumail.vic.gov.au | Mr              | William              | Smith                   | williamsmith2222@gmail.com| System             | 0469939394          | 0465547402       | Excellent      |
	Then I verify that the services is added successfully
	And I close the browser successfully

@service
Scenario Outline: EditService
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify that the service <service> can be searched
	And I edit a <service> with <telephone> and fill in details and submit
	#Then I verify that the service <service> is edited successfully
	And I close the browser successfully
	Examples:
	| service   | telephone  |
	| Ponds kids| 0469347402 |

@service
Scenario Outline: DeleteService
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify that the service <service> can be searched
	And I delete a <service>
	And I verify that the service <service> is deleted successfully
	And I close the browser successfully
	Examples:
	| service                  |
	| Moonee Kids Kindergarten |

# This feature will edit a non EYM service and apply for EYM in Kim sharepoint
@service
Scenario Outline: ApplyEym_ExistingService
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify that the service <service> can be searched
	And I edit a <service> for EYM funding
	And I apply for EYM organisation
	And I verify EYM funding status for <service> is submitted
	And I close the browser successfully
	Examples:
	| service       |
	| Ponds kids    |

# This feature will transfer an existing service to a different service provider
@service
Scenario Outline: Transfer_ExistingService
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify that the service <service> can be searched
	And I edit a <service> for transfer
	And I transfer a service to a different <Service Provider>
	And I close the browser successfully
	Examples:
	| service					| Service Provider      |
	| Moonee Kids Kindergarten	| Moreland City Council |


@service
Scenario Outline: RecommenceService
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify that the service <service> can be searched
	And I recommence a <service>
	And I fill recommence information
	| Legal Entity Name			| Rating	|
	| Altona West Kindergarten2	| Excellent	|
	And I verify <service> recommenced
	And I close the browser successfully
	Examples:
	| service							|
	| Narre Warren Central Preschool	|

@service
Scenario Outline: RecommenceService_asnotEYM_EymSP
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify that the service <service> can be searched
	And I recommence a <service>
	And I fill recommence information from EYM to not EYM
	| Number	| Street		| Street Type | Suburb      | State | Postcode | LGA Code         | Telephone  | Email                             | Title Emergency | Given Name Emergency | Family Name Emergency   | Email Emergency           | Position Emergency | Telephone Emergency | Mobile Emergency | Legal Entity Name			| Rating	|
	| 111		| Hoddy Street1	| ALLEY       | Monee Ponds | VIC   | 3007     | Alpine (S) 20110 | 0469347455 | sikka.niharika@edumail.vic.gov.au | Mr              | William              | Smith                   | williamsmith2222@gmail.com| System             | 0469939394          | 0465547402       | Altona West Kindergarten2	| Excellent	|
	And I verify <service> recommenced
	And I close the browser successfully
	Examples:
	| service						|
	| Williams Child Care Centre	|

@service
Scenario Outline: RecommenceService_asEYM_EymNotSUBMITTED_EymSP
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify that the service <service> can be searched
	And I recommence a <service>
	And I fill recommence information from EYM to EYM	
	| Number	| Street		| Street Type | Suburb      | State | Postcode | LGA Code         | Telephone  | Email                             | Title Emergency | Given Name Emergency | Family Name Emergency   | Email Emergency           | Position Emergency | Telephone Emergency | Mobile Emergency | Legal Entity Name			| Rating	|
	| 111		| Hoddy Street1	| ALLEY       | Monee Ponds | VIC   | 3007     | Alpine (S) 20110 | 0469347455 | sikka.niharika@edumail.vic.gov.au | Mr              | William              | Smith                   | williamsmith2222@gmail.com| System             | 0469939394          | 0465547402       | Altona West Kindergarten2	| Excellent	|
	And I verify <service> recommenced
	And I close the browser successfully
	Examples:
	| service								|
	| St Vincent's Early Learning Centre	|

@service
Scenario Outline: RecommenceService_asEYM_EymSUBMITTED_EymSP
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify that the service <service> can be searched
	And I recommence a <service>
	And I fill recommence information from EYM to EYM SUBMITTED
	| Number	| Street		| Street Type | Suburb      | State | Postcode | LGA Code         | Telephone  | Email                             | Title Emergency | Given Name Emergency | Family Name Emergency   | Email Emergency           | Position Emergency | Telephone Emergency | Mobile Emergency | Legal Entity Name			| Rating	|
	| 111		| Hoddy Street1	| ALLEY       | Monee Ponds | VIC   | 3007     | Alpine (S) 20110 | 0469347455 | sikka.niharika@edumail.vic.gov.au | Mr              | William              | Smith                   | williamsmith2222@gmail.com| System             | 0469939394          | 0465547402       | Altona West Kindergarten2	| Excellent	|
	And I verify <service> recommenced
	And I close the browser successfully
	Examples:
	| service                   |
	| Sandown Park Kindergarten |