// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:3.0.0.0
//      SpecFlow Generator Version:3.0.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace KIM_Automation.Features
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.0.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [TechTalk.SpecRun.FeatureAttribute("Service", Description="\tLogin to KIM1 and adding a new service to an organisation\r\n\tLogin to KIM1 and ad" +
        "ding multiple services to an organisation\r\n\tEdit an existing service\r\n\tRemove an" +
        " existing service", SourceFile="Features\\Service.feature", SourceLine=0)]
    public partial class ServiceFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "Service.feature"
#line hidden
        
        [TechTalk.SpecRun.FeatureInitialize()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "Service", "\tLogin to KIM1 and adding a new service to an organisation\r\n\tLogin to KIM1 and ad" +
                    "ding multiple services to an organisation\r\n\tEdit an existing service\r\n\tRemove an" +
                    " existing service", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [TechTalk.SpecRun.FeatureCleanup()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        public virtual void TestInitialize()
        {
        }
        
        [TechTalk.SpecRun.ScenarioCleanup()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("AddService", new string[] {
                "service"}, SourceLine=53)]
        public virtual void AddService()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("AddService", null, new string[] {
                        "service"});
#line 54
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 55
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 56
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 57
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 58
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 59
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 60
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table25 = new TechTalk.SpecFlow.Table(new string[] {
                        "Service Name",
                        "Date Commence",
                        "Unit Number",
                        "Street",
                        "Street Type",
                        "Suburb",
                        "State",
                        "Postcode",
                        "LGA Code",
                        "Telephone",
                        "Email",
                        "Title Emergency",
                        "Given Name Emergency",
                        "Family Name Emergency",
                        "Email Emergency",
                        "Position Emergency",
                        "Telephone Emergency",
                        "Mobile Emergency",
                        "Quality Rating"});
            table25.AddRow(new string[] {
                        "NCL Eym_No Service",
                        "17/06/2019",
                        "2",
                        "Tassell Street",
                        "ALLEY",
                        "Hadfield",
                        "VIC",
                        "3234",
                        "Alpine (S) 20110",
                        "0469347455",
                        "sikka.niharka@edumail.vic.gov.au",
                        "Mr",
                        "Will",
                        "Smith",
                        "williamsmith222@gmail.com",
                        "System",
                        "0469939394",
                        "0465547409",
                        "Excellent"});
#line 61
 testRunner.And("I add a new service and fill in details and submit", ((string)(null)), table25, "And ");
#line 64
 testRunner.Then("I verify that the service is added successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 65
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("AddService_EYM", new string[] {
                "service"}, SourceLine=71)]
        public virtual void AddService_EYM()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("AddService_EYM", null, new string[] {
                        "service"});
#line 72
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 73
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 74
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 75
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 76
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 77
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 78
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table26 = new TechTalk.SpecFlow.Table(new string[] {
                        "Service Name",
                        "Date Commence",
                        "Unit Number",
                        "Street",
                        "Street Type",
                        "Suburb",
                        "State",
                        "Postcode",
                        "LGA Code",
                        "Telephone",
                        "Email",
                        "Title Emergency",
                        "Given Name Emergency",
                        "Family Name Emergency",
                        "Email Emergency",
                        "Position Emergency",
                        "Telephone Emergency",
                        "Mobile Emergency",
                        "Quality Rating"});
            table26.AddRow(new string[] {
                        "NCL Eym3 Service",
                        "17/06/2019",
                        "2A",
                        "Tassell Street",
                        "ALLEY",
                        "Hadfield",
                        "VIC",
                        "3144",
                        "Alpine (S) 20110",
                        "0469347455",
                        "sikka.niharka@edumail.vic.gov.au",
                        "Mr",
                        "Will",
                        "Smith",
                        "williamsmith222@gmail.com",
                        "System",
                        "0469939394",
                        "0465547409",
                        "Excellent"});
#line 79
 testRunner.And("I add a new EYM service and fill in details and submit", ((string)(null)), table26, "And ");
#line 82
 testRunner.Then("I verify that the service is added successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 83
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("AddMultipleServices", new string[] {
                "service"}, SourceLine=85)]
        public virtual void AddMultipleServices()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("AddMultipleServices", null, new string[] {
                        "service"});
#line 86
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 87
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 88
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 89
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 90
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 91
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 92
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table27 = new TechTalk.SpecFlow.Table(new string[] {
                        "Service Name",
                        "Date Commence",
                        "Unit Number",
                        "Street",
                        "Street Type",
                        "Suburb",
                        "State",
                        "Postcode",
                        "LGA Code",
                        "Telephone",
                        "Email",
                        "Title Emergency",
                        "Given Name Emergency",
                        "Family Name Emergency",
                        "Email Emergency",
                        "Position Emergency",
                        "Telephone Emergency",
                        "Mobile Emergency",
                        "Quality Rating"});
            table27.AddRow(new string[] {
                        "Community Early-years Childcare - NC1",
                        "10/07/2019",
                        "222",
                        "Hoddy Street1",
                        "ALLEY",
                        "Monee Ponds",
                        "VIC",
                        "3141",
                        "Alpine (S) 20110",
                        "0469347455",
                        "sikka.niharika@edumail.vic.gov.au",
                        "Mr",
                        "William",
                        "Smith",
                        "williamsmith2222@gmail.com",
                        "System",
                        "0469939394",
                        "0465547402",
                        "Excellent"});
            table27.AddRow(new string[] {
                        "Community Early-years Childcare - NC2",
                        "10/07/2019",
                        "222",
                        "Hoddy Street2",
                        "ALLEY",
                        "Monee Ponds",
                        "VIC",
                        "3141",
                        "Alpine (S) 20110",
                        "0469347455",
                        "sikka.niharika@edumail.vic.gov.au",
                        "Mr",
                        "William",
                        "Smith",
                        "williamsmith2222@gmail.com",
                        "System",
                        "0469939394",
                        "0465547402",
                        "Excellent"});
#line 93
 testRunner.And("I add multiple services and fill in details and submit", ((string)(null)), table27, "And ");
#line 97
 testRunner.Then("I verify that the services is added successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 98
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        public virtual void EditService(string service, string telephone, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "service"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("EditService", null, @__tags);
#line 101
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 102
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 103
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 104
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 105
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 106
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 107
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 108
 testRunner.Then(string.Format("I verify that the service {0} can be searched", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 109
 testRunner.And(string.Format("I edit a {0} with {1} and fill in details and submit", service, telephone), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 111
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("EditService, Ponds kids", new string[] {
                "service"}, SourceLine=113)]
        public virtual void EditService_PondsKids()
        {
#line 101
this.EditService("Ponds kids", "0469347402", ((string[])(null)));
#line hidden
        }
        
        public virtual void DeleteService(string service, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "service"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("DeleteService", null, @__tags);
#line 117
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 118
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 119
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 120
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 121
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 122
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 123
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 124
 testRunner.Then(string.Format("I verify that the service {0} can be searched", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 125
 testRunner.And(string.Format("I delete a {0}", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 126
 testRunner.And(string.Format("I verify that the service {0} is deleted successfully", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 127
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("DeleteService, Moonee Kids Kindergarten", new string[] {
                "service"}, SourceLine=129)]
        public virtual void DeleteService_MooneeKidsKindergarten()
        {
#line 117
this.DeleteService("Moonee Kids Kindergarten", ((string[])(null)));
#line hidden
        }
        
        public virtual void ApplyEym_ExistingService(string service, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "service"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("ApplyEym_ExistingService", null, @__tags);
#line 134
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 135
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 136
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 137
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 138
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 139
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 140
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 141
 testRunner.Then(string.Format("I verify that the service {0} can be searched", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 142
 testRunner.And(string.Format("I edit a {0} for EYM funding", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 143
 testRunner.And("I apply for EYM organisation", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 144
 testRunner.And(string.Format("I verify EYM funding status for {0} is submitted", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 145
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("ApplyEym_ExistingService, Ponds kids", new string[] {
                "service"}, SourceLine=147)]
        public virtual void ApplyEym_ExistingService_PondsKids()
        {
#line 134
this.ApplyEym_ExistingService("Ponds kids", ((string[])(null)));
#line hidden
        }
        
        public virtual void Transfer_ExistingService(string service, string serviceProvider, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "service"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Transfer_ExistingService", null, @__tags);
#line 152
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 153
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 154
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 155
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 156
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 157
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 158
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 159
 testRunner.Then(string.Format("I verify that the service {0} can be searched", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 160
 testRunner.And(string.Format("I edit a {0} for transfer", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 161
 testRunner.And(string.Format("I transfer a service to a different {0}", serviceProvider), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 162
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("Transfer_ExistingService, Moonee Kids Kindergarten", new string[] {
                "service"}, SourceLine=164)]
        public virtual void Transfer_ExistingService_MooneeKidsKindergarten()
        {
#line 152
this.Transfer_ExistingService("Moonee Kids Kindergarten", "Moreland City Council", ((string[])(null)));
#line hidden
        }
        
        public virtual void RecommenceService(string service, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "service"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("RecommenceService", null, @__tags);
#line 169
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 170
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 171
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 172
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 173
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 174
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 175
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 176
 testRunner.Then(string.Format("I verify that the service {0} can be searched", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 177
 testRunner.And(string.Format("I recommence a {0}", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table28 = new TechTalk.SpecFlow.Table(new string[] {
                        "Legal Entity Name",
                        "Rating"});
            table28.AddRow(new string[] {
                        "Altona West Kindergarten2",
                        "Excellent"});
#line 178
 testRunner.And("I fill recommence information", ((string)(null)), table28, "And ");
#line 181
 testRunner.And(string.Format("I verify {0} recommenced", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 182
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("RecommenceService, Narre Warren Central Preschool", new string[] {
                "service"}, SourceLine=184)]
        public virtual void RecommenceService_NarreWarrenCentralPreschool()
        {
#line 169
this.RecommenceService("Narre Warren Central Preschool", ((string[])(null)));
#line hidden
        }
        
        public virtual void RecommenceService_AsnotEYM_EymSP(string service, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "service"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("RecommenceService_asnotEYM_EymSP", null, @__tags);
#line 188
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 189
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 190
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 191
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 192
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 193
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 194
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 195
 testRunner.Then(string.Format("I verify that the service {0} can be searched", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 196
 testRunner.And(string.Format("I recommence a {0}", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table29 = new TechTalk.SpecFlow.Table(new string[] {
                        "Number",
                        "Street",
                        "Street Type",
                        "Suburb",
                        "State",
                        "Postcode",
                        "LGA Code",
                        "Telephone",
                        "Email",
                        "Title Emergency",
                        "Given Name Emergency",
                        "Family Name Emergency",
                        "Email Emergency",
                        "Position Emergency",
                        "Telephone Emergency",
                        "Mobile Emergency",
                        "Legal Entity Name",
                        "Rating"});
            table29.AddRow(new string[] {
                        "111",
                        "Hoddy Street1",
                        "ALLEY",
                        "Monee Ponds",
                        "VIC",
                        "3007",
                        "Alpine (S) 20110",
                        "0469347455",
                        "sikka.niharika@edumail.vic.gov.au",
                        "Mr",
                        "William",
                        "Smith",
                        "williamsmith2222@gmail.com",
                        "System",
                        "0469939394",
                        "0465547402",
                        "Altona West Kindergarten2",
                        "Excellent"});
#line 197
 testRunner.And("I fill recommence information from EYM to not EYM", ((string)(null)), table29, "And ");
#line 200
 testRunner.And(string.Format("I verify {0} recommenced", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 201
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("RecommenceService_asnotEYM_EymSP, Williams Child Care Centre", new string[] {
                "service"}, SourceLine=203)]
        public virtual void RecommenceService_AsnotEYM_EymSP_WilliamsChildCareCentre()
        {
#line 188
this.RecommenceService_AsnotEYM_EymSP("Williams Child Care Centre", ((string[])(null)));
#line hidden
        }
        
        public virtual void RecommenceService_AsEYM_EymNotSUBMITTED_EymSP(string service, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "service"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("RecommenceService_asEYM_EymNotSUBMITTED_EymSP", null, @__tags);
#line 207
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 208
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 209
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 210
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 211
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 212
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 213
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 214
 testRunner.Then(string.Format("I verify that the service {0} can be searched", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 215
 testRunner.And(string.Format("I recommence a {0}", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table30 = new TechTalk.SpecFlow.Table(new string[] {
                        "Number",
                        "Street",
                        "Street Type",
                        "Suburb",
                        "State",
                        "Postcode",
                        "LGA Code",
                        "Telephone",
                        "Email",
                        "Title Emergency",
                        "Given Name Emergency",
                        "Family Name Emergency",
                        "Email Emergency",
                        "Position Emergency",
                        "Telephone Emergency",
                        "Mobile Emergency",
                        "Legal Entity Name",
                        "Rating"});
            table30.AddRow(new string[] {
                        "111",
                        "Hoddy Street1",
                        "ALLEY",
                        "Monee Ponds",
                        "VIC",
                        "3007",
                        "Alpine (S) 20110",
                        "0469347455",
                        "sikka.niharika@edumail.vic.gov.au",
                        "Mr",
                        "William",
                        "Smith",
                        "williamsmith2222@gmail.com",
                        "System",
                        "0469939394",
                        "0465547402",
                        "Altona West Kindergarten2",
                        "Excellent"});
#line 216
 testRunner.And("I fill recommence information from EYM to EYM", ((string)(null)), table30, "And ");
#line 219
 testRunner.And(string.Format("I verify {0} recommenced", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 220
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("RecommenceService_asEYM_EymNotSUBMITTED_EymSP, St Vincent\'s Early Learning Centre" +
            "", new string[] {
                "service"}, SourceLine=222)]
        public virtual void RecommenceService_AsEYM_EymNotSUBMITTED_EymSP_StVincentsEarlyLearningCentre()
        {
#line 207
this.RecommenceService_AsEYM_EymNotSUBMITTED_EymSP("St Vincent\'s Early Learning Centre", ((string[])(null)));
#line hidden
        }
        
        public virtual void RecommenceService_AsEYM_EymSUBMITTED_EymSP(string service, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "service"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("RecommenceService_asEYM_EymSUBMITTED_EymSP", null, @__tags);
#line 226
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 227
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 228
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 229
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 230
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 231
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 232
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 233
 testRunner.Then(string.Format("I verify that the service {0} can be searched", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 234
 testRunner.And(string.Format("I recommence a {0}", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table31 = new TechTalk.SpecFlow.Table(new string[] {
                        "Number",
                        "Street",
                        "Street Type",
                        "Suburb",
                        "State",
                        "Postcode",
                        "LGA Code",
                        "Telephone",
                        "Email",
                        "Title Emergency",
                        "Given Name Emergency",
                        "Family Name Emergency",
                        "Email Emergency",
                        "Position Emergency",
                        "Telephone Emergency",
                        "Mobile Emergency",
                        "Legal Entity Name",
                        "Rating"});
            table31.AddRow(new string[] {
                        "111",
                        "Hoddy Street1",
                        "ALLEY",
                        "Monee Ponds",
                        "VIC",
                        "3007",
                        "Alpine (S) 20110",
                        "0469347455",
                        "sikka.niharika@edumail.vic.gov.au",
                        "Mr",
                        "William",
                        "Smith",
                        "williamsmith2222@gmail.com",
                        "System",
                        "0469939394",
                        "0465547402",
                        "Altona West Kindergarten2",
                        "Excellent"});
#line 235
 testRunner.And("I fill recommence information from EYM to EYM SUBMITTED", ((string)(null)), table31, "And ");
#line 238
 testRunner.And(string.Format("I verify {0} recommenced", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 239
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("RecommenceService_asEYM_EymSUBMITTED_EymSP, Sandown Park Kindergarten", new string[] {
                "service"}, SourceLine=241)]
        public virtual void RecommenceService_AsEYM_EymSUBMITTED_EymSP_SandownParkKindergarten()
        {
#line 226
this.RecommenceService_AsEYM_EymSUBMITTED_EymSP("Sandown Park Kindergarten", ((string[])(null)));
#line hidden
        }
        
        [TechTalk.SpecRun.TestRunCleanup()]
        public virtual void TestRunCleanup()
        {
            TechTalk.SpecFlow.TestRunnerManager.GetTestRunner().OnTestRunEnd();
        }
    }
}
#pragma warning restore
#endregion
