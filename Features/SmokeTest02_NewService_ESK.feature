﻿Feature: SmokeTest02_NewSevice_ESK
Login to KIM1 and adding a new service to an organisation, run the jobs and approve in KIM2
Add ESK data(teachers, educators etc.) for added service in KIM1 as internal user
Login to manage jobs and run the batch jobs to get data flow from KIM1 to KIM2 and vice versa
Go to KIM2 and verify the newly added data
Navigate to KIM1 and check for all tabs completed

#######################################################################################
###
###This journey covers below:
###
###Creating a new service in kim sharepoint
###Run appropriate jobs
###Add all ESK related data
###Run crm messages to sharepoint job
###Verify that all data is completed on kim sharepoint for the particular service
###
###Please note:
###
###Commencement date should be appropriate
## Edit the Service Name/Service in table/s below.  
## I usually replace (1)date ddmmyy with today's date and (2)time hhmm with current time in in Service Name 
###RO_KimEditor and RO_KimApprover of appropriate regions
## Build/Rebuild
#######################################################################################
@smoke
Scenario: 01 AddService_ESK
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	And I add a new service and fill in details and submit	
	| Service Name     | Date Commence | Unit Number     | Street          | Street Type | Suburb      | State | Postcode        | LGA Code         | Telephone  | Email                            | Title Emergency | Given Name Emergency | Family Name Emergency | Email Emergency           | Position Emergency | Telephone Emergency | Mobile Emergency | Quality Rating |
	| NS11 Esk Service | "05/07/2019"  | <autogenerated> | <autogenerated> | ALLEY       | StrathmoreH | VIC   | <autogenerated> | Alpine (S) 20110 | 0469347455 | sikka.niharka@edumail.vic.gov.au | Mr              | Williams             | Smiths                | williamsmith222@gmail.com | System             | 0469939394          | 0465547409       | Excellent      |
	Then I verify that the service is added successfully
	And I close the browser successfully

@smoke
Scenario: 02 RunJob
	Given I go to manage job url
	When I run the services batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###Wait for specified miliseconds
Scenario Outline: 03 WaitScript
    Given I wait for <few> mins
Examples: 
	| few    |      
	| 150000 |

#################################################################################
##Please enter unique service approval id
##The below test works for only south western victoria region services
##In order to make it work for different region change region below 
## &
##Update the ROeditor userid and password in resources file
#################################################################################
Scenario Outline: 04 KimCrm_NewService_RO_KimEditor
	Given I login to Kim crm application as RO kim editor
	When As a Kim editor I search for added service <service>
	And editor completes the pending tasks
	| Service Region         | Service Category | Service Lga        | Service ApprovalId | Action                   |
	| South Western Vistoria | Standard         | Moonee Valley City | <autogenerated>    | Request Manager Approval |  
	##Then I logout of Kim crm application 
	Then I close the browser successfully
	Examples: 
	| service        |
	| NS11 Esk Service |  

##################################################################################
##Following test scenario runs for kim approver
##Login as kim regional approver and approves the new service
##################################################################################
Scenario Outline: 05 KimCrm_NewService_RO_KimApprover
	Given I login to Kim crm application as RO kim approver
	When As a Kim approver I search for added service <service>
	And approver approves the <service> with <status>
	##Then the service is ready for funding
	##And I logout of Kim crm application
	Then I close the browser successfully
	Examples: 
	| service        | status  |
	| NS11 Esk Service | Approve |  
	
	
@smoke
Scenario: 06 RunJob2
	Given I go to manage job url
	When I run the messages batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###Wait for specified miliseconds
Scenario Outline: 07 WaitScript
    Given I wait for <few> mins
Examples: 
	| few    |      
	| 100000 |

###Before running this script, it might be a good think to wait for 2 minutes before the job is run successfully
@smoke
Scenario Outline: 08 AddESkTeacher
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add ESK teachers
	| Given Name | Family Name | Gender | Date of Birth | CommencementDate | RegistrationStatus       | VITNumber | University                     | Course                                              | Year |
	| NS11     | HS        | Female | " 11/11/1995 "  | " 05/07/2019 "     | Fully registered teacher | 468384     | Australian Catholic University | Bachelor of Education (Early Childhood and Primary) | 2016 |
	And I close the browser successfully
	Examples:
	| service          |
	| NS11 Esk Service | 

@smoke
Scenario Outline: 09 AddESKProgram
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add Programs for ESK
	| Hours | Mins | Weeks | Fees | Fee Period |
	| 16    | 30   | 40    | 420  | Per term   |
	And I close the browser successfully
	Examples:
	| service        |
	| NS11 Esk Service | 

Scenario: 10 Run ESK wrapper job
	Given I go to manage job url
	When I run the esk wrapper job to import programs, child etc to kim crm
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###Wait for specified miliseconds
Scenario Outline: 11 WaitScript
    Given I wait for <few> mins
Examples: 
	| few    |      
	| 25000 |

###The teacher name should be changed appropraitely as per first script
###Child Commencement Date should be after service commencement date
@smoke
Scenario Outline: 12 AddESKEnrolment
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add enrolments for ESK
	| Given Name      | Family Name     | Sex    | Date of Birth   | Number          | Street          | StreetType | Suburb   | State | Postcode        | ChildCommence  | EarlyStart KinderEligibility    | Early Childhood Teacher | Hours | Minutes | weeks | Immunisation                                                         |
	| <autogenerated> | <autogenerated> | Female | <autogenerated> | <autogenerated> | <autogenerated> | STEPS      | essendon | VIC   | <autogenerated> | " 05/07/2019 " | Child known to Child Protection | NS11 HS - ESK           | 16    | 30      | 30    | The child has an up to date immunisation status certificate recorded |
	And I close the browser successfully
	Examples:
	| service        |
	| NS11 Esk Service | 

Scenario: 13 Run ESK wrapper job
	Given I go to manage job url
	When I run the esk wrapper job to import programs, child etc to kim crm
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###Wait for specified miliseconds
Scenario Outline: 14 WaitScript
    Given I wait for <few> mins
Examples: 
	| few    |      
	| 15000 |

Scenario: 15 RunJob2
	Given I go to manage job url
	When I run the messages batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###Wait for specified miliseconds
Scenario Outline: 16 WaitScript
    Given I wait for <few> mins
Examples: 
	| few    |      
	| 15000 |

###To verify if all the ESk tabs are completed
Scenario Outline: 17 Confirm ESK complete
    Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I check if all ESK tabs are complete
	And I close the browser successfully
	Examples:
	| service        |
	| NS11 Esk Service | 

############################################### END OF SCRIPT ###################################################