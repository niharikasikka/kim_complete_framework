// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:3.0.0.0
//      SpecFlow Generator Version:3.0.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace KIM_Automation.Features
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.0.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [TechTalk.SpecRun.FeatureAttribute("SmokeTest02_NewSevice_ESK", Description=@"Login to KIM1 and adding a new service to an organisation, run the jobs and approve in KIM2
Add ESK data(teachers, educators etc.) for added service in KIM1 as internal user
Login to manage jobs and run the batch jobs to get data flow from KIM1 to KIM2 and vice versa
Go to KIM2 and verify the newly added data
Navigate to KIM1 and check for all tabs completed", SourceFile="Features\\SmokeTest02_NewService_ESK.feature", SourceLine=0)]
    public partial class SmokeTest02_NewSevice_ESKFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "SmokeTest02_NewService_ESK.feature"
#line hidden
        
        [TechTalk.SpecRun.FeatureInitialize()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "SmokeTest02_NewSevice_ESK", @"Login to KIM1 and adding a new service to an organisation, run the jobs and approve in KIM2
Add ESK data(teachers, educators etc.) for added service in KIM1 as internal user
Login to manage jobs and run the batch jobs to get data flow from KIM1 to KIM2 and vice versa
Go to KIM2 and verify the newly added data
Navigate to KIM1 and check for all tabs completed", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [TechTalk.SpecRun.FeatureCleanup()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        public virtual void TestInitialize()
        {
        }
        
        [TechTalk.SpecRun.ScenarioCleanup()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("01 AddService_ESK", new string[] {
                "smoke"}, SourceLine=26)]
        public virtual void _01AddService_ESK()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("01 AddService_ESK", null, new string[] {
                        "smoke"});
#line 27
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 28
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 29
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 30
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 31
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 32
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 33
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table38 = new TechTalk.SpecFlow.Table(new string[] {
                        "Service Name",
                        "Date Commence",
                        "Unit Number",
                        "Street",
                        "Street Type",
                        "Suburb",
                        "State",
                        "Postcode",
                        "LGA Code",
                        "Telephone",
                        "Email",
                        "Title Emergency",
                        "Given Name Emergency",
                        "Family Name Emergency",
                        "Email Emergency",
                        "Position Emergency",
                        "Telephone Emergency",
                        "Mobile Emergency",
                        "Quality Rating"});
            table38.AddRow(new string[] {
                        "NS11 Esk Service",
                        "\"05/07/2019\"",
                        "<autogenerated>",
                        "<autogenerated>",
                        "ALLEY",
                        "StrathmoreH",
                        "VIC",
                        "<autogenerated>",
                        "Alpine (S) 20110",
                        "0469347455",
                        "sikka.niharka@edumail.vic.gov.au",
                        "Mr",
                        "Williams",
                        "Smiths",
                        "williamsmith222@gmail.com",
                        "System",
                        "0469939394",
                        "0465547409",
                        "Excellent"});
#line 34
 testRunner.And("I add a new service and fill in details and submit", ((string)(null)), table38, "And ");
#line 37
 testRunner.Then("I verify that the service is added successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 38
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("02 RunJob", new string[] {
                "smoke"}, SourceLine=40)]
        public virtual void _02RunJob()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("02 RunJob", null, new string[] {
                        "smoke"});
#line 41
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 42
 testRunner.Given("I go to manage job url", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 43
 testRunner.When("I run the services batch job", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 44
 testRunner.Then("the service is successfully run", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 45
 testRunner.And("I logout of manage jobs", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 46
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        public virtual void _03WaitScript(string few, string[] exampleTags)
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("03 WaitScript", null, exampleTags);
#line 49
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 50
    testRunner.Given(string.Format("I wait for {0} mins", few), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("03 WaitScript, 150000", SourceLine=52)]
        public virtual void _03WaitScript_150000()
        {
#line 49
this._03WaitScript("150000", ((string[])(null)));
#line hidden
        }
        
        public virtual void _04KimCrm_NewService_RO_KimEditor(string service, string[] exampleTags)
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("04 KimCrm_NewService_RO_KimEditor", null, exampleTags);
#line 62
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 63
 testRunner.Given("I login to Kim crm application as RO kim editor", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 64
 testRunner.When(string.Format("As a Kim editor I search for added service {0}", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
            TechTalk.SpecFlow.Table table39 = new TechTalk.SpecFlow.Table(new string[] {
                        "Service Region",
                        "Service Category",
                        "Service Lga",
                        "Service ApprovalId",
                        "Action"});
            table39.AddRow(new string[] {
                        "South Western Vistoria",
                        "Standard",
                        "Moonee Valley City",
                        "<autogenerated>",
                        "Request Manager Approval"});
#line 65
 testRunner.And("editor completes the pending tasks", ((string)(null)), table39, "And ");
#line 69
 testRunner.Then("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("04 KimCrm_NewService_RO_KimEditor, NS11 Esk Service", SourceLine=71)]
        public virtual void _04KimCrm_NewService_RO_KimEditor_NS11EskService()
        {
#line 62
this._04KimCrm_NewService_RO_KimEditor("NS11 Esk Service", ((string[])(null)));
#line hidden
        }
        
        public virtual void _05KimCrm_NewService_RO_KimApprover(string service, string status, string[] exampleTags)
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("05 KimCrm_NewService_RO_KimApprover", null, exampleTags);
#line 78
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 79
 testRunner.Given("I login to Kim crm application as RO kim approver", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 80
 testRunner.When(string.Format("As a Kim approver I search for added service {0}", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 81
 testRunner.And(string.Format("approver approves the {0} with {1}", service, status), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 84
 testRunner.Then("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("05 KimCrm_NewService_RO_KimApprover, NS11 Esk Service", SourceLine=86)]
        public virtual void _05KimCrm_NewService_RO_KimApprover_NS11EskService()
        {
#line 78
this._05KimCrm_NewService_RO_KimApprover("NS11 Esk Service", "Approve", ((string[])(null)));
#line hidden
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("06 RunJob2", new string[] {
                "smoke"}, SourceLine=90)]
        public virtual void _06RunJob2()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("06 RunJob2", null, new string[] {
                        "smoke"});
#line 91
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 92
 testRunner.Given("I go to manage job url", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 93
 testRunner.When("I run the messages batch job", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 94
 testRunner.Then("the service is successfully run", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 95
 testRunner.And("I logout of manage jobs", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 96
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        public virtual void _07WaitScript(string few, string[] exampleTags)
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("07 WaitScript", null, exampleTags);
#line 99
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 100
    testRunner.Given(string.Format("I wait for {0} mins", few), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("07 WaitScript, 100000", SourceLine=102)]
        public virtual void _07WaitScript_100000()
        {
#line 99
this._07WaitScript("100000", ((string[])(null)));
#line hidden
        }
        
        public virtual void _08AddESkTeacher(string service, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "smoke"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("08 AddESkTeacher", null, @__tags);
#line 107
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 108
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 109
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 110
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 111
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 112
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 113
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 114
 testRunner.Then(string.Format("I verify the {0} is added successfully to the organisation in Kim sharepoint", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            TechTalk.SpecFlow.Table table40 = new TechTalk.SpecFlow.Table(new string[] {
                        "Given Name",
                        "Family Name",
                        "Gender",
                        "Date of Birth",
                        "CommencementDate",
                        "RegistrationStatus",
                        "VITNumber",
                        "University",
                        "Course",
                        "Year"});
            table40.AddRow(new string[] {
                        "NS11",
                        "HS",
                        "Female",
                        "\" 11/11/1995 \"",
                        "\" 05/07/2019 \"",
                        "Fully registered teacher",
                        "468384",
                        "Australian Catholic University",
                        "Bachelor of Education (Early Childhood and Primary)",
                        "2016"});
#line 115
 testRunner.And("I add ESK teachers", ((string)(null)), table40, "And ");
#line 118
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("08 AddESkTeacher, NS11 Esk Service", new string[] {
                "smoke"}, SourceLine=120)]
        public virtual void _08AddESkTeacher_NS11EskService()
        {
#line 107
this._08AddESkTeacher("NS11 Esk Service", ((string[])(null)));
#line hidden
        }
        
        public virtual void _09AddESKProgram(string service, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "smoke"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("09 AddESKProgram", null, @__tags);
#line 124
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 125
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 126
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 127
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 128
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 129
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 130
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 131
 testRunner.Then(string.Format("I verify the {0} is added successfully to the organisation in Kim sharepoint", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            TechTalk.SpecFlow.Table table41 = new TechTalk.SpecFlow.Table(new string[] {
                        "Hours",
                        "Mins",
                        "Weeks",
                        "Fees",
                        "Fee Period"});
            table41.AddRow(new string[] {
                        "16",
                        "30",
                        "40",
                        "420",
                        "Per term"});
#line 132
 testRunner.And("I add Programs for ESK", ((string)(null)), table41, "And ");
#line 135
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("09 AddESKProgram, NS11 Esk Service", new string[] {
                "smoke"}, SourceLine=137)]
        public virtual void _09AddESKProgram_NS11EskService()
        {
#line 124
this._09AddESKProgram("NS11 Esk Service", ((string[])(null)));
#line hidden
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("10 Run ESK wrapper job", SourceLine=139)]
        public virtual void _10RunESKWrapperJob()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("10 Run ESK wrapper job", null, ((string[])(null)));
#line 140
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 141
 testRunner.Given("I go to manage job url", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 142
 testRunner.When("I run the esk wrapper job to import programs, child etc to kim crm", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 143
 testRunner.Then("the service is successfully run", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 144
 testRunner.And("I logout of manage jobs", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 145
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        public virtual void _11WaitScript(string few, string[] exampleTags)
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("11 WaitScript", null, exampleTags);
#line 148
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 149
    testRunner.Given(string.Format("I wait for {0} mins", few), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("11 WaitScript, 25000", SourceLine=151)]
        public virtual void _11WaitScript_25000()
        {
#line 148
this._11WaitScript("25000", ((string[])(null)));
#line hidden
        }
        
        public virtual void _12AddESKEnrolment(string service, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "smoke"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("12 AddESKEnrolment", null, @__tags);
#line 157
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 158
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 159
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 160
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 161
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 162
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 163
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 164
 testRunner.Then(string.Format("I verify the {0} is added successfully to the organisation in Kim sharepoint", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            TechTalk.SpecFlow.Table table42 = new TechTalk.SpecFlow.Table(new string[] {
                        "Given Name",
                        "Family Name",
                        "Sex",
                        "Date of Birth",
                        "Number",
                        "Street",
                        "StreetType",
                        "Suburb",
                        "State",
                        "Postcode",
                        "ChildCommence",
                        "EarlyStart KinderEligibility",
                        "Early Childhood Teacher",
                        "Hours",
                        "Minutes",
                        "weeks",
                        "Immunisation"});
            table42.AddRow(new string[] {
                        "<autogenerated>",
                        "<autogenerated>",
                        "Female",
                        "<autogenerated>",
                        "<autogenerated>",
                        "<autogenerated>",
                        "STEPS",
                        "essendon",
                        "VIC",
                        "<autogenerated>",
                        "\" 05/07/2019 \"",
                        "Child known to Child Protection",
                        "NS11 HS - ESK",
                        "16",
                        "30",
                        "30",
                        "The child has an up to date immunisation status certificate recorded"});
#line 165
 testRunner.And("I add enrolments for ESK", ((string)(null)), table42, "And ");
#line 168
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("12 AddESKEnrolment, NS11 Esk Service", new string[] {
                "smoke"}, SourceLine=170)]
        public virtual void _12AddESKEnrolment_NS11EskService()
        {
#line 157
this._12AddESKEnrolment("NS11 Esk Service", ((string[])(null)));
#line hidden
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("13 Run ESK wrapper job", SourceLine=172)]
        public virtual void _13RunESKWrapperJob()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("13 Run ESK wrapper job", null, ((string[])(null)));
#line 173
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 174
 testRunner.Given("I go to manage job url", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 175
 testRunner.When("I run the esk wrapper job to import programs, child etc to kim crm", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 176
 testRunner.Then("the service is successfully run", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 177
 testRunner.And("I logout of manage jobs", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 178
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        public virtual void _14WaitScript(string few, string[] exampleTags)
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("14 WaitScript", null, exampleTags);
#line 181
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 182
    testRunner.Given(string.Format("I wait for {0} mins", few), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("14 WaitScript, 15000", SourceLine=184)]
        public virtual void _14WaitScript_15000()
        {
#line 181
this._14WaitScript("15000", ((string[])(null)));
#line hidden
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("15 RunJob2", SourceLine=186)]
        public virtual void _15RunJob2()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("15 RunJob2", null, ((string[])(null)));
#line 187
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 188
 testRunner.Given("I go to manage job url", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 189
 testRunner.When("I run the messages batch job", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 190
 testRunner.Then("the service is successfully run", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 191
 testRunner.And("I logout of manage jobs", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 192
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        public virtual void _16WaitScript(string few, string[] exampleTags)
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("16 WaitScript", null, exampleTags);
#line 195
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 196
    testRunner.Given(string.Format("I wait for {0} mins", few), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("16 WaitScript, 15000", SourceLine=198)]
        public virtual void _16WaitScript_15000()
        {
#line 195
this._16WaitScript("15000", ((string[])(null)));
#line hidden
        }
        
        public virtual void _17ConfirmESKComplete(string service, string[] exampleTags)
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("17 Confirm ESK complete", null, exampleTags);
#line 202
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 203
    testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 204
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 205
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 206
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 207
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 208
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 209
 testRunner.Then(string.Format("I verify the {0} is added successfully to the organisation in Kim sharepoint", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 210
 testRunner.And("I check if all ESK tabs are complete", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 211
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("17 Confirm ESK complete, NS11 Esk Service", SourceLine=213)]
        public virtual void _17ConfirmESKComplete_NS11EskService()
        {
#line 202
this._17ConfirmESKComplete("NS11 Esk Service", ((string[])(null)));
#line hidden
        }
        
        [TechTalk.SpecRun.TestRunCleanup()]
        public virtual void TestRunCleanup()
        {
            TechTalk.SpecFlow.TestRunnerManager.GetTestRunner().OnTestRunEnd();
        }
    }
}
#pragma warning restore
#endregion
