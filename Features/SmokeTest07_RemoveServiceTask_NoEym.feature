﻿Feature: SmokeTest07_RemoveServiceTask_NoEym
	Remove a service from kim sharepoint for a not approved Eym funded service
	Run the necessary jobs to get messages in crm
	Review CSK-3 (EC Service – Approval of request to remove a service from K1) as kim region approver

	###################################################################################
	##
	##Pre conditions:
	##
	##A non eym funded service with appropriate cease date of service
	##And there are no funded child records for the current year
	##or
	##A non eym ready for funding service
	##The service should exist in kim sharepoint
	##
	##Please note: Cease date should be current date or future date
	####################################################################################

###Remove a non eym service with cease date as current date
Scenario Outline: 01 DeleteService_NoEym_CeaseServiceTask
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify that the service <service> can be searched
	And I delete a <service>
	And I verify that the service <service> is deleted successfully
	And I close the browser successfully
	Examples:
	| service       |
	|Mock1 Test   | 

###Manage jobs url to get the sharepoint to crm messages
Scenario: 02 RunJob_MessagesKimCrm
	Given I go to manage job url
	When I run the services batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###RO Kim approver approves the service cease request and mark the task complete
Scenario Outline: 03 KimCrm_CeaseService_RO_KimApprover_WithTask
	Given I login to Kim crm application as RO kim approver
	When As a Kim approver I search for added service <service>
	And approver approves the <service> with <status>
	Then Kim approver marks the <task> complete
	##And I logout of Kim crm application
	And I close the browser successfully
	Examples: 
	| service     | status  | task                                            |
	| Mock1 Test  | Approve | Approval of request to remove a service from K1 |

###Wait for 3-4 mins before running next script
@smoke7
Scenario: 04 RunJob2
	Given I go to manage job url
	When I run the messages batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

Scenario Outline: 05 WaitScript
    Given I wait for <few> mins
Examples: 
	| few    |      
	| 100000 |

###After the job is succesfully run verify that service is deleted successfully in kim sharepoint application
Scenario Outline: 06 Verify service does not appear in kim sharepoint application
Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify that the service <service> can be searched
	And I verify that the service <service> is deleted from kim sharepoint
	And I close the browser successfully
	Examples:
	| service     |
	|Mock1 Test   |