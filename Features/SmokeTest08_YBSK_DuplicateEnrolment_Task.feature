﻿Feature: SmokeTest08_YBSK_DuplicateEnrolment_Task
	Create multiple YBSK enrolment in Kim sharepoint application
	The enrolments should be duplicate under one service or different service
	Run the required jobs
	Review UKC-2 task (UKC-2	Child – possible duplicate record)

	#####################################################################
	##
	##Please note: YBSK enrolments should be duplicate 
	##
	##Update teacher name as per added YBSK teacher list
	##DOB should be for YBSK enrolment
	######################################################################

###Please update the group name as per the service program added
##Unique names, dob, street address to create unique enrolments
##Below script creates ybsk enrolment so dob should be appropriate
###Create a duplicate enrolment
@enrolment
Scenario Outline: 01 AddEnrolment_YBSK
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add enrolments for YBSK
	| Given Name | Family Name | Sex  | Date of Birth  | Number | Street | StreetType | Suburb   | State | Postcode | DateCommenced  | language | Immunisation                                                         | GroupAttended | AdultA High           | AdultA Qualification     | AdultA Occupation | AdultB HIgh           | AdultB Qualification     | AdultB Occupation |
	| James      | Whittam     | Male | " 04/10/2014 " | 7      | Tas    | STEPS      | Hadfield | VIC   | 3142     | " 26/06/2019 " | Dutch    | The child has an up to date immunisation status certificate recorded | Orange Group  | Year 12 or equivalent | Bachelor degree or above | A                 | Year 12 or equivalent | Bachelor degree or above | A                 |
	And I close the browser successfully
	Examples:
	| service  |
	| NS Service |

##To import YBSK data to Kim Crm
Scenario: 02 Run YBSK wrapper job
	Given I go to manage job url
	When I run the ybsk wrapper job to import programs, child etc to kim crm
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

##Run the enrolment confirmation job
Scenario: 03 Run Enrolment Confirmation job
	Given I go to manage job url
	When I run the enrolment confirmation job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

##Run the message job from kim crm to sharepoint
Scenario: 04 Run KIM crm message to KIM sharepoint
	Given I go to manage job url
	When I run the messages batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

##Adding the duplicate enrolment under different service
##Name, DOB and gender should be same for duplicate enrolments
##SLK generated for both should be same
##Scenario Outline: 05 AddDuplicateEnrolment_YBSK
##	Given I navigate to Kim sharepoint aplication homepage
##	And I login using internal username and password
##	When I press login button
##	And I am logged in successfully
##	And I click on service providers
##	And I search for an organisation and click on it
##	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
##	And I add enrolments for YBSK
##	| Given Name | Family Name | Sex  | Date of Birth  | Number | Street | StreetType | Suburb   | State | Postcode | DateCommenced  | language | Immunisation                                                         | GroupAttended | AdultA High           | AdultA Qualification     | AdultA Occupation | AdultB HIgh           | AdultB Qualification     | AdultB Occupation |
##	| James      | Whittam     | Male | " 04/10/2014 " | 7      | Tas    | STEPS      | Hadfield | VIC   | 3142     | " 26/06/2019 " | Dutch    | The child has an up to date immunisation status certificate recorded | Orange Group  | Year 12 or equivalent | Bachelor degree or above | A                 | Year 12 or equivalent | Bachelor degree or above | A                 |
##	And I close the browser successfully
##	Examples:
##	| service    |
##	| Mock1 Test | 

##To import YBSK data to Kim Crm
##Scenario: 06 Run YBSK wrapper job
##	Given I go to manage job url
##	When I run the ybsk wrapper job to import programs, child etc to kim crm
##	Then the service is successfully run
##	And I logout of manage jobs
##	And I close the browser successfully

##Run the enrolment confirmation job
##IMPORTANT:
##After creating duplicate enrolments, this job can take upto 30 minutes. Wait for the time before moving to next steps
##Scenario: 07 Run Enrolment Confirmation job
##	Given I go to manage job url
##	When I run the enrolment confirmation job
##	Then the service is successfully run
##	And I logout of manage jobs
##	And I close the browser successfully

##As RO kim editor, complete the pending task and mark as complete
Scenario Outline: 08 KimCrm_DuplicateEnrol_RO_KimEditor
	Given I login to Kim crm application as RO kim editor
	When As a Kim editor I search for added service <service>
	When Kim editor navigate to duplicate enrolment and go to activities
	| Children - SLK | note                                          |
	| HITAM041020141 | Duplicate record informed to service provider |
	Then Kim editor marks the <task> complete for enrolment 
	Then I logout of Kim crm application 
	And I close the browser successfully
	Examples: 
	| service     | task                              |
	| Mock1 Test| Child – possible duplicate record |