﻿Feature: SmokeTest09_AnnualConfirmation_Tasks

#################################################################################################################################################################################################
##This scenario verifies below tasks for Annual Confirmation on kim crm:
##
##ECF-13	Annual confirmation – No Qualified Teacher
##ECF-15	Annual confirmation – less than 600 hours
##ECF-36	Annual confirmation – no written confirmation to parents
##
##Pre conditions:
##Existing service with completed teachers, educators, programs and enrolments
##
###Steps include:
##
###Complete annual confirmation data without ticking the checkboxes for below 3 tasks
##
##Early Childhood teaching qualification not on the approved ACECQA list or other qualification
##Each child is enrolled and attending a kindergarten program that is offered for a minimum of 15 hours of kindergarten per week for a minimum of 40 weeks per year (or 600 hours per year)
##Written confirmation has been/will be provided to parents that their child will receive a funded kindergarten program at this service
##
##Run necessary integration jobs
###The below scenario verifies the task , do necessary steps and marks the task as complete 
##################################################################################################################################################################################################

##Pre condition:
##
##Programs is sessional with 4yo selected
##Teachers, educators, programs and enrolments tabs are completed

@smoke09
@annualconfirmation
Scenario Outline: 01 AnnualConfirm_Sessional_4yo
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I check the status for Teachers, Educators, Program, and Enrolments tabs all COMPLETE
	Then I verify the status for Annual Confirmation should be COMPLETE
	And I click on annual confirmation tab
	Then I verify the available fields in annual confirmation tab
	And I verify the values in annual confirmation tab
	And I click confirm and submit the data for sessional program with not checked checkboxes
	| TLDS completed | 0 TLDS completed forwarded to schools | 3 year old children enrolled | no of hours for 3 year old | Total Other Early | Masters or above | Graduate diploma | Dual bachelors | Bachelor | Pathway | Diploma | CertName | CertPosition | CertDate   |
	| 0              | 0                                     | 0                            | 0                          | 0                 | 0                | 0                | 0              | 0        | 0       | 0       | TestName | TestPosition | 01/08/2013 |
	Then I verify the status for Teachers, Educators, Program, and Enrolments tabs all Read Only
	And I close the browser successfully	
	Examples:
	| service    |
	| NC Service |  

@smoke09
### To initiate the enrolment confirmation process for an EC Service
Scenario: 02 Run Enrolment Confirmation job
	Given I go to manage job url
	When I run the enrolment confirmation job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

@smoke09
###Wait for specified miliseconds
Scenario Outline: 03 WaitScript
    Given I wait for <few> mins
Examples: 
	| few    |      
	| 100000 |


@smoke09
##As regional Kim editor enter a note for the task: Annual confirmation – No Qualified Teacher 
##Mark the task as complete
Scenario Outline: 04 KimCrm_RO_KimEditor
	Given I login to Kim crm application as RO kim editor
	When As a Kim editor I search for added service <service>
	And Kim RO editor completes the task with <noteTeacher>
	Then Kim editor marks the <NoQualifiedTeacher> complete
	##And I logout of Kim crm application
	And I close the browser successfully
	Examples:
	| service    | NoQualifiedTeacher                         | noteTeacher                     |
	| NC Service| Annual confirmation – No Qualified Teacher | Teacher qualifications modified | 

@smoke09
##As regional Kim approver status update to approve
Scenario Outline: 05 KimCrm_AnnualConfTasks_RO_KimApprover
	Given I login to Kim crm application as RO kim approver
	When As a Kim approver I search for added service <service>
	And Kim approver checks for data and changes the status to Approve
	##Then I logout of Kim crm application
	Then I close the browser successfully
	Examples:
	| service    |
	| NC Service | 

@smoke09
##As regional Kim editor enter a note for the task: Annual confirmation – less than 600 hours
##Mark the task as complete
Scenario Outline: 06 KimCrm_RO_KimEditor
	Given I login to Kim crm application as RO kim editor
	When As a Kim editor I search for added service <service>
	When Kim RO editor completes the task with <noteNumberHours>
	Then Kim editor marks the <LessHours> complete
	##And I logout of Kim crm application
	And I close the browser successfully
	Examples:
	| service    | LessHours                                 | noteNumberHours                      |
	| NC Service | Annual confirmation – less than 600 hours | Service confirms more than 600 hours | 

@smoke09
##As regional Kim approver status update to approve
Scenario Outline: 07 KimCrm_AnnualConfTasks_RO_KimApprover
	Given I login to Kim crm application as RO kim approver
	When As a Kim approver I search for added service <service>
	And Kim approver checks for data and changes the status to Approve
	##Then I logout of Kim crm application
	Then I close the browser successfully
	Examples:
	| service  |
	|NC Service | 

@smoke09
##As regional Kim editor enter a note for the task: Annual confirmation – no written confirmation to parents
##Mark the task as complete
Scenario Outline: 08 KimCrm_RO_KimEditor
	Given I login to Kim crm application as RO kim editor
	When As a Kim editor I search for added service <service>
	When Kim RO editor completes the task with <noteWrittenComm>
	Then Kim editor marks the <NoWrittenComm> complete
	##And I logout of Kim crm application
	And I close the browser successfully
	Examples:
	| service  | NoWrittenComm                                            | noteWrittenComm                           |
	|NC Service | Annual confirmation – no written confirmation to parents | Written communication to parents provided | 

@smoke09
##As regional Kim approver status update to approve
Scenario Outline: 09 KimCrm_AnnualConfTasks_RO_KimApprover
	Given I login to Kim crm application as RO kim approver
	When As a Kim approver I search for added service <service>
	And Kim approver checks for data and changes the status to Approve
	##Then I logout of Kim crm application
	Then I close the browser successfully
	Examples:
	| service    |
	| NC Service | 