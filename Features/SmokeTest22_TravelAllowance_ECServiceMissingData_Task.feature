﻿Feature: SmokeTest22_TravelAllowance_ECServiceMissingData_Task
	Travel Allowance submit via K1 as service provider
	Run the appropriate job
	Check the travel allowance in Kim crm and view related task/s
	Review TAF-4 Travel Allowance – EC Service missing data	
	
#################################################################################################################################################################################################
## This automation is used to verify the following
## Teacher can apply for Travel Allowance
## Can see the Travel Allowance application
##
## THINGS TO DO/NOTE:
## Teacher is under a service with missing information for Service Category or missing information for both License ID and Service Approval ID
## Service used below is for Moonee Valley City Council which is setup in Resource.resx
## Update the ServiceTravel depending on the service provider used in your test
## Edit the Service Name - should be updated based on test performed
## Name refers to teacher name so should be updated accordingly
##
#################################################################################################################################################################################################
Scenario Outline: 01 ApplicationTravelAllowance_Kim1
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I open an existing teacher to apply for Travel Allowance with <name>
	And  I fill teachers application for travel leave
	| ServiceTravel      | FirstDay_Travel | Weeks_Year1 | Weeks_Year2 | Kms | Name         | Position       |
	| NCL SP110719_80256 | "21/06/2019"    | 5           | 4           | 10  | Scott Martin | Senior Manager |
	And I close the browser successfully
	Examples:
	| service            | name                                  |
	| NCL SP150719_00745 | Teacher150719_0745 William150719_0745 | 

###To initiate the travel allowance leave job
###The job imports the travel leave to kim crm
Scenario: 02 TravelAllowance_Job
	Given I go to manage job url
	When I run the travel leave job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###Wait for specified miliseconds
Scenario Outline: 03 WaitScript
    Given I wait for <few> mins
Examples: 
	| few   |
	| 60000 |

Scenario Outline: 04 Verify Travel Allowance and view related task for missing data for EC Service
	Given I login to Kim crm application as CO kim editor
	When As a Kim editor I search for added service <service>
	And I check the Workforce Leave Approval if Travel Allowance application was added	
	| name                                  | Funding Type     | Funding Status |
	| Teacher150719_0745 William150719_0745 | Travel Allowance | Pending        |
	Then Kim editor view the <task>
	##And I logout of Kim crm application
	##And I close the browser successfully
	Examples:
	| service            | task                                       |
	| NCL SP150719_00745 | Travel Allowance – EC Service missing data |