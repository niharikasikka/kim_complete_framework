﻿Feature: SmokeTest23_ExistingService_ApprovePreCommitments

#############################################################################################
###
##For existing service, approve the pre commitments as RO kim approver
###
##Verify all the tabs in KIM1 and checks the adjustments
###
##############################################################################################

##The below scenario works for a new service which is not confirmed
##Status of the servie is In Progress
    ##| Kindergarten Fee Subsidy | 864710007	|
	##| ESK EXT CP               | 864710007	|
	## below are the corresponding value for each action
	## 864710007 - Approve
	## 864710005 - More Information
	## 864710002 - Reject to Region
	## 864710001 - Request Central Office Approval

####Please Note: This script sometimes does not run in IE because element is not identified in IE
@smoke23
Scenario Outline: 01 KimCrmPreCommitments_ApprovePrecommitment
	Given I login to Kim crm application as RO kim approver
	When As a Kim approver I search for added service <service>
	Then I verify the confirm <status> of funding information
	And I approve the following precommitments and funding category
	| Funding Category         | Action		|	
	| Per Capita               | 864710007	|
	##And I logout of Kim crm application
	And I close the browser successfully
Examples: 
	| service    | status      |
	| NS Service | In Progress | 

@smoke23
Scenario: 02 Run KIM crm message to KIM sharepoint
	Given I go to manage job url
	When I run the messages batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

@smoke23
###Wait for specified miliseconds
Scenario Outline: 03 WaitScript
    Given I wait for <few> mins
Examples: 
	| few    |      
	| 100000 |

@smoke23
Scenario Outline: 04 Adjustments_Available
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I verify the status for Teachers, Educators, Program, Enrolments, and Annual Confirmation tabs are all COMPLETE
	And I verify the adjustment tab status should be available
	And I click on adjustments tab
	Then I verify the available fields in Adjustments tab
	And I verify the correctness of values in Adjustments tab
	And I close the browser successfully
	Examples:
	| service    |
	| NS Service | 

######################### END OF SCRIPT ################################################################