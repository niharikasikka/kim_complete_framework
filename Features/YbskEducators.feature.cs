// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:3.0.0.0
//      SpecFlow Generator Version:3.0.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace KIM_Automation.Features
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.0.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [TechTalk.SpecRun.FeatureAttribute("YbskEducators", Description="\tAdd an educator to a service or to multiple services\r\n\tAdd multiple educators to" +
        " a service or multiple services\r\n\tEdit an existing educator to a service\r\n\tRemov" +
        "e an existing educator from a service by name", SourceFile="Features\\YbskEducators.feature", SourceLine=0)]
    public partial class YbskEducatorsFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "YbskEducators.feature"
#line hidden
        
        [TechTalk.SpecRun.FeatureInitialize()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "YbskEducators", "\tAdd an educator to a service or to multiple services\r\n\tAdd multiple educators to" +
                    " a service or multiple services\r\n\tEdit an existing educator to a service\r\n\tRemov" +
                    "e an existing educator from a service by name", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [TechTalk.SpecRun.FeatureCleanup()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        public virtual void TestInitialize()
        {
        }
        
        [TechTalk.SpecRun.ScenarioCleanup()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void AddEducator(string service, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "educator"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("AddEducator", null, @__tags);
#line 27
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 28
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 29
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 30
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 31
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 32
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 33
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 34
 testRunner.Then(string.Format("I verify the {0} is added successfully to the organisation in Kim sharepoint", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            TechTalk.SpecFlow.Table table120 = new TechTalk.SpecFlow.Table(new string[] {
                        "Given Name",
                        "Family Name",
                        "Gender",
                        "Date of Birth",
                        "HighestTeachingQualification",
                        "IndustrialAgreement",
                        "Level",
                        "ProfDevHours",
                        "HoursofEmployment",
                        "HoursWorkedinAnyprogram"});
            table120.AddRow(new string[] {
                        "Tobi",
                        "Half",
                        "Male",
                        "\" 01/09/1988 \"",
                        "Kindercraft Assistant",
                        "A local council agreement with EEEA appended",
                        "1.1",
                        "80",
                        "40",
                        "60"});
#line 35
 testRunner.And("I add educators", ((string)(null)), table120, "And ");
#line 38
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("AddEducator, Moe P.L.A.C.E.", new string[] {
                "educator"}, SourceLine=40)]
        public virtual void AddEducator_MoeP_L_A_C_E_()
        {
#line 27
this.AddEducator("Moe P.L.A.C.E.", ((string[])(null)));
#line hidden
        }
        
        public virtual void AddMultipleEducator(string service, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "educator"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("AddMultipleEducator", null, @__tags);
#line 48
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 49
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 50
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 51
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 52
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 53
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 54
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 55
 testRunner.Then(string.Format("I verify the {0} is added successfully to the organisation in Kim sharepoint", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            TechTalk.SpecFlow.Table table121 = new TechTalk.SpecFlow.Table(new string[] {
                        "Given Name",
                        "Family Name",
                        "Gender",
                        "Date of Birth",
                        "HighestTeachingQualification",
                        "IndustrialAgreement",
                        "Level",
                        "ProfDevHours",
                        "HoursofEmployment",
                        "HoursWorkedinAnyprogram"});
            table121.AddRow(new string[] {
                        "Timm1",
                        "Hafner1",
                        "Male",
                        "\" 25/06/1984 \"",
                        "Kindercraft Assistant",
                        "A local council agreement with EEEA appended",
                        "1.1",
                        "80",
                        "40",
                        "50"});
            table121.AddRow(new string[] {
                        "Tomm1",
                        "Hafner1",
                        "Male",
                        "\" 25/06/1984 \"",
                        "Kindercraft Assistant",
                        "A local council agreement with EEEA appended",
                        "1.1",
                        "80",
                        "40",
                        "20"});
#line 56
 testRunner.And("I add educators", ((string)(null)), table121, "And ");
#line 60
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("AddMultipleEducator, Happy Kids", new string[] {
                "educator"}, SourceLine=62)]
        public virtual void AddMultipleEducator_HappyKids()
        {
#line 48
this.AddMultipleEducator("Happy Kids", ((string[])(null)));
#line hidden
        }
        
        public virtual void EditEducator(string service, string name, string dob, string hours, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "educator"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("EditEducator", null, @__tags);
#line 69
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 70
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 71
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 72
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 73
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 74
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 75
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 76
 testRunner.Then(string.Format("I verify the {0} is added successfully to the organisation in Kim sharepoint", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 77
 testRunner.And(string.Format("I open an existing educator with {0}", name), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 78
 testRunner.And(string.Format("I edit {0} and number of employment {1} of an educator", dob, hours), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 79
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("EditEducator, Moonee Kids Kindergarten", new string[] {
                "educator"}, SourceLine=81)]
        public virtual void EditEducator_MooneeKidsKindergarten()
        {
#line 69
this.EditEducator("Moonee Kids Kindergarten", "Tobby Hafneri", "\" 29/04/1991 \"", "70", ((string[])(null)));
#line hidden
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("EditEducator, Happy Kids", new string[] {
                "educator"}, SourceLine=81)]
        public virtual void EditEducator_HappyKids()
        {
#line 69
this.EditEducator("Happy Kids", "Tob Hafner", "\" 29/01/1990 \"", "90", ((string[])(null)));
#line hidden
        }
        
        public virtual void DeleteEducator(string service, string name, string reason, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "educator"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("DeleteEducator", null, @__tags);
#line 89
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 90
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 91
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 92
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 93
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 94
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 95
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 96
 testRunner.Then(string.Format("I verify the {0} is added successfully to the organisation in Kim sharepoint", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 97
 testRunner.And(string.Format("I delete an existing educator {0} with {1}", name, reason), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 98
 testRunner.And(string.Format("I verify that educator {0} is deleted successfully", name), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 99
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("DeleteEducator, Moonee Kids Kindergarten", new string[] {
                "educator"}, SourceLine=101)]
        public virtual void DeleteEducator_MooneeKidsKindergarten()
        {
#line 89
this.DeleteEducator("Moonee Kids Kindergarten", "Tobi Half", "Resigned", ((string[])(null)));
#line hidden
        }
        
        public virtual void ShowInactiveMakeitActiveEducator(string service, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "educator"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("ShowInactiveMakeitActiveEducator", null, @__tags);
#line 108
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 109
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 110
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 111
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 112
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 113
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 114
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 115
 testRunner.Then(string.Format("I verify the {0} is added successfully to the organisation in Kim sharepoint", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 116
 testRunner.And("I add click show inactive and make inactive educator to active", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 117
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("ShowInactiveMakeitActiveEducator, The Grange Kindergarten", new string[] {
                "educator"}, SourceLine=119)]
        public virtual void ShowInactiveMakeitActiveEducator_TheGrangeKindergarten()
        {
#line 108
this.ShowInactiveMakeitActiveEducator("The Grange Kindergarten", ((string[])(null)));
#line hidden
        }
        
        public virtual void CheckthenumberofTeacher(string service, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "educator"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("CheckthenumberofTeacher", null, @__tags);
#line 123
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 124
 testRunner.Given("I navigate to Kim sharepoint aplication homepage", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 125
 testRunner.And("I login using internal username and password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 126
 testRunner.When("I press login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 127
 testRunner.And("I am logged in successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 128
 testRunner.And("I click on service providers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 129
 testRunner.And("I search for an organisation and click on it", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 130
 testRunner.Then(string.Format("I verify the {0} is added successfully to the organisation in Kim sharepoint", service), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 131
 testRunner.And("I check the number of educator in Other Educators tab", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 132
 testRunner.And("I close the browser successfully", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("CheckthenumberofTeacher, NCL SP2706010318", new string[] {
                "educator"}, SourceLine=134)]
        public virtual void CheckthenumberofTeacher_NCLSP2706010318()
        {
#line 123
this.CheckthenumberofTeacher("NCL SP2706010318", ((string[])(null)));
#line hidden
        }
        
        [TechTalk.SpecRun.TestRunCleanup()]
        public virtual void TestRunCleanup()
        {
            TechTalk.SpecFlow.TestRunnerManager.GetTestRunner().OnTestRunEnd();
        }
    }
}
#pragma warning restore
#endregion
