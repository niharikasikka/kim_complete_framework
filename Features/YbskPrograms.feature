﻿Feature: YbskPrograms
	Add a program to a service or to multiple services
	Add multiple programs to a service or multiple services
	Edit an existing program to a service
	Remove an existing program from a service

	# Program type: Sessional for 4 year old
	# Group name should be unique
@program
Scenario Outline: AddProgram_Sessional_4yo
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add sessional program for four year
	| Group Name   | Start    | End      | FeeCharged | FeePeriod |
	| Orange Group | 08:30 AM | 04:45 PM | 350.00     | Term      |
	And I click on submit for processing
	And I close the browser successfully
	Examples:
	| service                       |
	| Elizabeth Wilmot Kindergarten |

	# Adding multiple program type, make sure it is one type of program
	# Program type: Sessional for 4 year old
@program
Scenario Outline: AddPrograms_Sessional_4yo
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add programs when programs are set to sessional
	| Group Name    | Start    | End      | FeeCharged | FeePeriod |
	| Purple1 Group | 08:30 AM | 04:30 PM | 150.00     | Term      |
	| Mouve1 Group  | 08:45 AM | 04:45 PM | 250.00     | Term      |
	And I click on submit for processing
	And I close the browser successfully
	Examples:
	| service   |
	| Ponds kids|

@program
Scenario Outline: EditProgram
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I open an existing program with name <name>
	And I edit fees <fees> charged of program
	And I verify that program is successfully edited <name>
	And I close the browser successfully
	Examples:
	| service    | name             | fees    |
	| Ponds kids | Blue12 Group     | 300.00  | 

@program
Scenario Outline: DeletePrograms
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I delete an existing program with name <name>
	And I confirm removal of group <name>
	And I close the browser successfully
	Examples:
	| service    | name  |
	| Ponds kids | 22    |

@program
Scenario Outline: AddProgram_Sessional_4yo3yo_Combined
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add a sessional program for four and three year combined
	| Group Name    | Start    | End      | FeeCharged | FeePeriod |
	| Orange1 Group | 08:30 AM | 04:45 PM | 350.00     | Term      | 
	And I click on submit for processing
	And I close the browser successfully
	Examples:
	| service                       |
	| Elizabeth Wilmot Kindergarten |

@program
Scenario Outline: AddProgram_Sessional_4yo3yo_Separate
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add a sessional program for four and three year separate
	| Group Name    | Start    | End      | FeeCharged | FeePeriod |
	| Orange2 Group | 08:30 AM | 04:45 PM | 350.00     | Term      |
	And I click on submit for processing
	And I close the browser successfully
	Examples:
	| service                       |
	| Moore Street Kindergarten | 

@program
Scenario Outline: AddProgram_Sessional_Rotational_4yo
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add a sessional program with rotational model for four year old and room <room>
	| Group Name    | Start    | End      | FeeCharged | FeePeriod |
	| Orange2 Group | 08:30 AM | 04:45 PM | 350.00     | Term      |
	And I click on submit for processing
	And I close the browser successfully
	Examples:
	| service                  | room        |
	| Murray Road Kindergarten | room yellow | 

@program
Scenario Outline: AddProgram_Sessional_Rotational_4yo3yo_Combined
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add a sessional program with rotational model for four and three year old combined and room <room>
	| Group Name    | Start    | End      | FeeCharged | FeePeriod |
	| Green Group | 08:30 AM | 04:45 PM | 350.00     | Term      |
	And I click on submit for processing
	And I close the browser successfully
	Examples:
	| service                  | room        |
	| Murray Road Kindergarten | room yellow2 | 

@program
Scenario Outline: AddProgram_Sessional_Rotational_4yo3yo_Separate
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add a sessional program with rotational model for four year and three year old separate and room <room>
	| Group Name    | Start    | End      | FeeCharged | FeePeriod |
	| Blue Group | 08:30 AM | 04:45 PM | 350.00     | Term      |
	And I click on submit for processing
	And I close the browser successfully
	Examples:
	| service                  | room         |
	| Murray Road Kindergarten | room yellow1 |

@program
Scenario Outline: AddProgram_Integrated_With3yoProgram
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add integrated program with three year old program and number of <weeks>
	| Teacher Name      | Start | End   | Day     |
	| FstN3830 LstN3830 | 08:30 | 16:45 | Monday  |
	| FstN3830 LstN3830 | 08:45 | 16:30 | Tuesday | 
	And I click on submit for processing
	And I close the browser successfully
	Examples:
	| service                       | weeks |
	| Sara Court Kindergarten | 10    |

@program
Scenario Outline: AddProgram_Integrated_Without3yoProgram
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add integrated program without three year old program and number of <weeks>
	| Teacher Name      | Start | End   | Day     |
	| FstN3830 LstN3830 | 08:30 | 16:45 | Monday  |
	| FstN3830 LstN3830 | 08:45 | 16:30 | Tuesday | 
	And I click on submit for processing
	And I close the browser successfully
	Examples:
	| service                       | weeks |
	| Sara Court Kindergarten | 15    |

@program
Scenario Outline: AddProgram_SessionalIntegrated_WithRotational_4yo
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add both integrated and sessional programs with rotational program and four year old program and number of <weeks> 
	| Teacher Name      | Start | End   | Day     |
	| FstN3830 LstN3830 | 08:30 | 16:45 | Monday  |
	| FstN3830 LstN3830 | 08:45 | 16:30 | Tuesday | 
	And I add a sessional program also and room <room>
	| Group Name | Start    | End      | FeeCharged | FeePeriod |
	| Brown Group    | 08:30 AM | 04:45 PM | 350.00     | Term      |
	And I click on submit for processing
	And I close the browser successfully
	Examples:
	| service                 | weeks | room |
	| Sara Court Kindergarten | 10    | Room R   |

@program
Scenario Outline: AddProgram_SessionalIntegrated_WithoutRotational_4yo
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add both integrated and sessional programs without rotational program and four year old program and number of <weeks>
	| Teacher Name      | Start | End   | Day     |
	| FstN3830 LstN3830 | 08:30 | 16:45 | Monday  |
	| FstN3830 LstN3830 | 08:45 | 16:30 | Tuesday | 
	And I add a sessional program also
	| Group Name | Start    | End      | FeeCharged | FeePeriod |
	| A Group | 08:30 AM | 04:45 PM | 350.00     | Term      |
	And I click on submit for processing
	And I close the browser successfully
	Examples:
	| service                       | weeks |
	| Elizabeth Wilmot Kindergarten | 15    |

	@program
Scenario Outline: LinkEnrolment_Program
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint	
	And I click link enrolment to an existing program with <name>	
	And I close the browser successfully
	Examples:
	| service               | name         |
	| Oakbrook Kindergarten | Rotational A |