﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Data;
using System.Threading;
using TechTalk.SpecFlow;

namespace KIM_Automation.Pages
{
    class ESKProgramPage : BaseApplicationPage
    {
        public ESKProgramPage(IWebDriver driver) : base(driver)
        { }
        public By EarlyStartKindergarten_Element => By.XPath("//input[@class='btn blue float-left']");
        public By ESKProgram_Element => By.XPath("//strong[contains(text(),'Program')]");

        public By Hours_Element => By.CssSelector("#eskProgramHrs4Week");
        public By Minutes_Element => By.CssSelector("#eskProgramMins4Week");
        public By Weeks_Element => By.CssSelector("#eskProgramWeeks4Yr");
        public By Fees_Element => By.CssSelector("#eskProgramFee");
        public IWebElement FeePeriod_dropdown => Driver.FindElement(By.XPath("//select[@id='eskProgramPeriod']"));
        public By SubmitBtn_Element => By.CssSelector("#SubmitESKProgramBtn");
        public By ProgramTabStatusElement => By.XPath("//span[@class='status program-tab-status']");

        internal void FillESKProgramForm(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));

                wait.Until(x => x.FindElement(Hours_Element));
                Driver.FindElement(Hours_Element).Clear();
                Driver.FindElement(Hours_Element).SendKeys(row.ItemArray[0].ToString());

                wait.Until(x => x.FindElement(Minutes_Element));
                Driver.FindElement(Minutes_Element).Clear();
                Driver.FindElement(Minutes_Element).SendKeys(row.ItemArray[1].ToString());

                wait.Until(x => x.FindElement(Weeks_Element));
                Driver.FindElement(Weeks_Element).Clear();
                Driver.FindElement(Weeks_Element).SendKeys(row.ItemArray[2].ToString());

                wait.Until(x => x.FindElement(Fees_Element));
                Driver.FindElement(Fees_Element).Clear();
                Driver.FindElement(Fees_Element).SendKeys(row.ItemArray[3].ToString());

                SelectElement FeePeriod = new SelectElement(FeePeriod_dropdown);
                FeePeriod.SelectByText(row.ItemArray[4].ToString());

                wait.Until(x => x.FindElement(SubmitBtn_Element));
                Driver.FindElement(SubmitBtn_Element).Click();
            }

        }

        internal void AddNewESKProgram()
        {

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.FindElement(EarlyStartKindergarten_Element).Click();

            wait.Until(x => x.FindElement(ESKProgram_Element));
            Driver.FindElement(ESKProgram_Element).Click();
        }

        internal void EskProgramStatus()
        {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
                //verify Annual Tab status
                wait.Until(x => x.FindElement(ProgramTabStatusElement));
                string strprogramTabStatus = Driver.FindElement(ProgramTabStatusElement).Text;
                if (strprogramTabStatus.Equals("Complete"))
                {

                }
                else
                {
                    throw new Exception("Fail: Program tab status is " + strprogramTabStatus);
                };
            }
    }
}