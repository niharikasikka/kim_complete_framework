﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading.Tasks;

namespace KIM_SmokeTest.Pages
{
    class Kim1Homepage : BaseApplicationPage
    {
        public Kim1Homepage(IWebDriver driver) : base(driver)
        { }
        public By ActualText_Element => By.XPath("//h1[contains(text(),'Kindergarten Information Management')]");
        public IWebElement UserId_Element => Driver.FindElement(By.CssSelector("#username"));
        public IWebElement Password_Element => Driver.FindElement(By.XPath("//input[@id='password']"));
        public IWebElement Submit_Element => Driver.FindElement(By.XPath("//input[@id='SubmitCreds']"));
        public IWebElement ServiceProvider_Element => Driver.FindElement(By.XPath("//a[contains(text(),'Service Providers')]"));
        public IWebElement LogOutMenu_Element => Driver.FindElement(By.XPath("//span[@id='zz16_Menu_t']"));
        public IWebElement LogOut_Element =>  Driver.FindElement(By.XPath("//a[@id='mp1_0_1_Anchor']"));
        public IWebElement Reports_Element => Driver.FindElement(By.XPath("//a[contains(text(),'Reports')]"));

        internal void GetUrl()
        {
            string url = CommonFunctions.GetResourceValue("Kim1Url_dev");
            Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(80);
            //Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(80);
            Driver.Navigate().GoToUrl(url);
        }

        internal void EnterUsername()
        {
            string userName = CommonFunctions.GetResourceValue("Kim1UserId_Internal");
            //Enter user id of the user
            UserId_Element.SendKeys(userName);
            
        }

        internal void EnterPassword()
        {

            string password = CommonFunctions.GetResourceValue("Kim1Password_Internal");
            Password_Element.SendKeys(password);  
        }

        internal void ClickButton()
        { 
            Submit_Element.Click();
        }

        internal void LoggedIn()
        {
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(60));
            waitAgency.Until(c => c.FindElement(ActualText_Element));
            Assert.AreEqual("Kindergarten Information Management", Driver.FindElement(ActualText_Element).Text.ToString());
        }

        internal void ServiceProviderClick()
        {
            ServiceProvider_Element.Click();
        }

        internal void ClickReports()
        {
            Reports_Element.Click();
            Task.Delay(10000).Wait();
        }
    }
}