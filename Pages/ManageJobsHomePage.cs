﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace KIM_Automation.Pages
{
    namespace KIM_Automation.Pages
    {
        class ManageJobsHomePage : BaseApplicationPage
        {
            public ManageJobsHomePage(IWebDriver driver) : base(driver)
            { }
            public IWebElement UserId_Element => Driver.FindElement(By.CssSelector("#UserName"));
            public IWebElement Password_Element => Driver.FindElement(By.CssSelector("#Password"));
            public IWebElement SignIn_Element => Driver.FindElement(By.CssSelector("#cmdSubmit"));
            public IWebElement SearchJob_Element => Driver.FindElement(By.CssSelector("#JobFilter"));
            public IWebElement FilterButton_Element => Driver.FindElement(By.CssSelector("#btnFilter"));
            public IWebElement Next_Element => Driver.FindElement(By.XPath("//a[contains(text(),'Next')]"));
            public By RunJob_Element => By.XPath("//a[@class='btn btn-info runNow']");
            public IWebElement ViewJob_Element => Driver.FindElement(By.XPath("//a[@class='btn btn-default']"));
            public IWebElement LogOut_Element => Driver.FindElement(By.XPath("//a[contains(text(),'Logout')]"));
            public By RunConfirmButton_Element => By.CssSelector("#runNowConfirm");
            public IWebElement JobList_Element => Driver.FindElement(By.XPath("//ul[@class='dropdown-menu']//li//a"));
            public IWebElement Job_Element => Driver.FindElement(By.CssSelector("#menuTestLabel"));
            public IWebElement StatusJob => Driver.FindElement(By.XPath("//a[contains(text(),'Scheduled')]"));


            internal void GetUrl()
            {       
                string url = CommonFunctions.GetResourceValue("ManageJobs_Url");
                Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(60);
                Driver.Navigate().GoToUrl(url);
            }

            internal string ManageJobsSignIn()
            {
                Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
                string UserName = CommonFunctions.GetResourceValue("ManageJobsUsername");
                UserId_Element.SendKeys(UserName);
                string Password = CommonFunctions.GetResourceValue("ManageJobsPassword");
                Password_Element.SendKeys(Password); 
                SignIn_Element.Click();
                var actualText = Driver.Title.ToString();
                //Assert.IsTrue(actualText.Contains("Manage jobs"), "User not on manage jobs page");
                return actualText.ToString();
            }

            internal void SearchJobName()
            {
                for (int i=0; i<5; i++) {
                    Thread.Sleep(3000);
                    //Next_Element.Click();
                    SearchJob_Element.SendKeys(CommonFunctions.GetResourceValue("JobName1"));
                    FilterButton_Element.Click();
                    Thread.Sleep(5000);
                    // Find the job and run it
                    if (ViewJob_Element.Displayed)
                    {
                        ViewJob_Element.Click();
                        WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
                        waitAgency.Until(c => c.FindElement(RunJob_Element));
                        Driver.FindElement(RunJob_Element).Click();
                        Thread.Sleep(5000);
                        Driver.FindElement(RunConfirmButton_Element).Click();
                        break;
                    }
                    else
                    {
                        Next_Element.Click();
                        Thread.Sleep(2000);
                    }
                }
               
            }

            internal void LogOut()
            {
                Thread.Sleep(2000);
                CommonFunctions.GetClick(LogOut_Element);
                Thread.Sleep(5000);
            }

            internal void SearchJobName1()
            {

                for (int i = 0; i < 3; i++)
                {
                    Thread.Sleep(3000);
                    //Next_Element.Click();
                    SearchJob_Element.SendKeys(CommonFunctions.GetResourceValue("JobName2"));
                    FilterButton_Element.Click();
                    Thread.Sleep(5000);
                    // Find the job and run it
                    if (ViewJob_Element.Displayed)
                    {
                        ViewJob_Element.Click();
                        WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
                        waitAgency.Until(c => c.FindElement(RunJob_Element));
                        Driver.FindElement(RunJob_Element).Click();
                        Thread.Sleep(5000);
                        Driver.FindElement(RunConfirmButton_Element).Click();
                        break;
                    }
                    else
                    {
                        Next_Element.Click();
                        Thread.Sleep(2000);
                    }
                }
            }

            internal void ServiceRunSuccess()
            {
                Assert.IsTrue(Driver.FindElement(RunJob_Element).Displayed, "Service not run successfully");
            }

            internal void SearchEskJobName()
            {
                SearchJob_Element.SendKeys(CommonFunctions.GetResourceValue("EskJobName"));
                FilterButton_Element.Click();
          
                ViewJob_Element.Click();
                WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
                waitAgency.Until(c => c.FindElement(RunJob_Element));
                Driver.FindElement(RunJob_Element).Click();
                waitAgency.Until(c => c.FindElement(RunConfirmButton_Element));
                Thread.Sleep(2000);
                Driver.FindElement(RunConfirmButton_Element).Click();
                Thread.Sleep(2000);
            }

            internal void SearchYbskJobName()
            {
                SearchJob_Element.SendKeys(CommonFunctions.GetResourceValue("YbskJobName"));
                FilterButton_Element.Click();

                ViewJob_Element.Click();
                WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
                waitAgency.Until(c => c.FindElement(RunJob_Element));
                Driver.FindElement(RunJob_Element).Click();
                waitAgency.Until(c => c.FindElement(RunConfirmButton_Element));
                Thread.Sleep(2000);
                Driver.FindElement(RunConfirmButton_Element).Click();
                Thread.Sleep(2000);
            }

            internal void GoToJob_Verify()
            {
                for (int i = 0; i < 3; i++)
                {
                    Thread.Sleep(3000);
                    
                    // Find the job and run it
                    if (Driver.PageSource.Contains("Queued"))
                    {
                        Thread.Sleep(30000);
                       
                    }
                    else
                    {
                        Thread.Sleep(2000);
                        Next_Element.Click();
                        Thread.Sleep(2000);
                    }
                }
            }

            internal void SearchEnrolmentConfJob()
            {
                SearchJob_Element.SendKeys(CommonFunctions.GetResourceValue("AnnualConfirmation"));
                FilterButton_Element.Click();
                //Thread.Sleep(2000);
                ViewJob_Element.Click();
                WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
                waitAgency.Until(c => c.FindElement(RunJob_Element));
                Driver.FindElement(RunJob_Element).Click();
                //Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
                //waitAgency.Until(c => c.FindElement(RunConfirmButton_Element));
                Thread.Sleep(2000);
                Driver.FindElement(RunConfirmButton_Element).Click();
                Thread.Sleep(2000);
            }
            public IWebElement Jobs_Link => Driver.FindElement(By.XPath("//span[@id='menuTestLabel']"));
            public IWebElement JobList_Link => Driver.FindElement(By.XPath("//ul[@class='dropdown-menu']"));
            internal void Navigate_JobList()
            {
                Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
                Jobs_Link.Click();
                JobList_Link.Click();
                Thread.Sleep(2000);
            }

            internal void SearchParentalJob()
            {
                SearchJob_Element.SendKeys(CommonFunctions.GetResourceValue("ParentalLeave"));
                FilterButton_Element.Click();
                //Thread.Sleep(2000);
                ViewJob_Element.Click();
                WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
                waitAgency.Until(c => c.FindElement(RunJob_Element));
                Driver.FindElement(RunJob_Element).Click();
                //Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
                //waitAgency.Until(c => c.FindElement(RunConfirmButton_Element));
                Thread.Sleep(2000);
                Driver.FindElement(RunConfirmButton_Element).Click();
                Thread.Sleep(2000);
            }

            internal void SearchTravelJob()
            {
                SearchJob_Element.SendKeys(CommonFunctions.GetResourceValue("TravelAllowance_Job"));
                FilterButton_Element.Click();
                //Thread.Sleep(2000);
                ViewJob_Element.Click();
                WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
                waitAgency.Until(c => c.FindElement(RunJob_Element));
                Driver.FindElement(RunJob_Element).Click();
                //Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
                //waitAgency.Until(c => c.FindElement(RunConfirmButton_Element));
                Thread.Sleep(2000);
                Driver.FindElement(RunConfirmButton_Element).Click();
                Thread.Sleep(2000);
            }
        }
    }
}