﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using TechTalk.SpecFlow;

namespace KIM_SmokeTest.FeatureSteps
{
    internal class ProgramsPage : BaseApplicationPage
    {
        public ProgramsPage(IWebDriver driver) : base(driver)
        { }
        //Programs Elements
        public By Programs_Element => By.XPath("//strong[contains(text(),'Programs')]");
        public By Edit_Element => By.XPath("//button[@class='btn blue program']");
        //Rotational Model-radio btn
        public By RotationalSessionalModel => By.XPath("//form[@id='programSetUpForm']//div[@class='fieldRow']//input[@id='sessional']");
        public By IntegratedModel => By.XPath("//form[@id='programSetUpForm']//div[@class='fieldRow']//input[@id='longDay']");
        public By SessionalIntegratedModel => By.XPath("//form[@id='programSetUpForm']//div[@class='fieldRow']//input[@id='solBoth']");
        public IWebElement Kindergarten_Weeks => Driver.FindElement(By.XPath("//form[@id='programSetUpForm']//input[@id='setupproOpWeeks']"));
        public By RotationalModel_Deliver_No => By.XPath("//form[@id='programSetUpForm']//input[@id='rotationalModelNo']");
        public By RotationalModel_Deliver_Yes => By.XPath("//form[@id='programSetUpForm']//input[@id='rotationalModelYes']");
        public By ESKProgram_Integrated_Yes => By.XPath("//form[@id='programSetUpForm']//input[@id='setup3YrOldProgramYes']");
        public By ESKProgram_Integrated_No => By.XPath("//form[@id='programSetUpForm']//input[@id='setup3YrOldProgramNo']");
        //Save Btn
        By SaveBtn_Programs => By.XPath("//form[@id='programSetUpForm']//input[@name='btnSave']");
        public By AddSessionalGroup_Element => By.XPath("//button[@class='btn blue sessional']");
        public IWebElement GroupName_Element => Driver.FindElement(By.CssSelector("#proSessGroupName"));
        public IWebElement RoomName_Element => Driver.FindElement(By.XPath("//div[@id='sessionAddRoomModal']//div[@class='modal-dialog']//input[@id='sessionRoomName']"));
        public IWebElement Ok_Link => Driver.FindElement(By.XPath("//div[@id='sessionAddRoomModal']//div[@class='modal-dialog']//input[@name='btnSaveRoom']"));
        public IWebElement AddRoom => Driver.FindElement(By.XPath("//input[@id='btnAddRoom']"));
        public By FeesCharged_Element => By.CssSelector("#proGroupFees1");

        //Selecting Start Time from dropdown
        public By Start_Element => By.CssSelector("#grpSessStart1");
        // Selecting End Time from dropdown
        public By End_Element => By.CssSelector("#grpSessFinish1");
        //Selecting Fee Period from dropdown
        public By FeePeriod_Element => By.CssSelector("#proFeePeriod1");
        //List box
        public By Listbox_Element => By.XPath("//div[@id='ms-ddlSessionalTeachers']//div[@class='ms-selectable']//ul[@class='ms-list']");
        public By LB_Select => By.XPath("/ html[1] / body[1] / form[1] / div[5] / div[1] / div[1] / div[1] / div[6] / div[2] / span[5] / div[1] / div[2] / div[3] / div[8] / div[1] / div[1] / form[1] / div[3] / div[1] / div[2] / div[1] / div[3] / div[1] / div[1] / ul[1] / li[1] / span[1]");
        //Save Btn
        By SaveBtn_SessionalGroup => By.XPath("//form[@id='programAddEditForm']//input[@name='btnSave']");
        public By FourYearsOnly_Radio => By.XPath("//form[@id='programSetUpForm']//input[@id='3or4YrOldProgram1']");
        public By Fouryo3yo_Combined_Radio => By.XPath("//form[@id='programSetUpForm']//input[@id='3or4YrOldProgram3']");
        public By Fouryo3yo_Separate_Radio => By.XPath("//form[@id='programSetUpForm']//input[@id='3or4YrOldProgram2']");
        public By SubmitProcessing_Button => By.XPath("//input[@id='SubmitProgramBtn']");
        public By SearchPrograms_Element => By.XPath("//div[@id='DataTables_Table_1_filter']//input");
        public By RemovePrograms_Link => By.XPath("//tr[@class='odd']//button[@class='btn blue remove'][contains(text(),'Remove')]");
        public By ExitBtn_SessionalGroup => By.XPath("//form[@id='programAddEditForm']//button[@name='btnExit'][contains(text(),'Exit')]");
        public By LinkEnrolmentForm => By.XPath("//form[@id='linkEnrolmentsForm']");
        public By LinkEnrolment_AvailableSelect => By.CssSelector("select#1592858269-selectable");
        public By LinkBtn_LinkEnrolment => By.XPath("//input[@class='btn blue btn-primary link submit']");
        internal void Programs_Sessional_4yo()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            Thread.Sleep(3000);
            wait.Until(x => x.FindElement(Edit_Element));
            Driver.FindElement(Edit_Element).Click();
            Thread.Sleep(2000);
            wait.Until(x => x.FindElement(RotationalSessionalModel));
            Driver.FindElement(RotationalSessionalModel).Click();
            wait.Until(x => x.FindElement(RotationalModel_Deliver_No));
            Driver.FindElement(RotationalModel_Deliver_No).Click();
            wait.Until(x => x.FindElement(FourYearsOnly_Radio));
            Driver.FindElement(FourYearsOnly_Radio).Click();
            wait.Until(x => x.FindElement(SaveBtn_Programs));
            Driver.FindElement(SaveBtn_Programs).Click();

        }
        internal void Programs_Sessional_4yo3yo_Combined()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            Thread.Sleep(3000);
            wait.Until(x => x.FindElement(Edit_Element));
            Driver.FindElement(Edit_Element).Click();
            Thread.Sleep(2000);
            wait.Until(x => x.FindElement(RotationalSessionalModel));
            Driver.FindElement(RotationalSessionalModel).Click();
            wait.Until(x => x.FindElement(RotationalModel_Deliver_No));
            Driver.FindElement(RotationalModel_Deliver_No).Click();
            wait.Until(x => x.FindElement(Fouryo3yo_Combined_Radio));
            Driver.FindElement(Fouryo3yo_Combined_Radio).Click();
            wait.Until(x => x.FindElement(ESKProgram_Yes));
            Driver.FindElement(ESKProgram_Yes).Click();
            wait.Until(x => x.FindElement(SaveBtn_Programs));
            Driver.FindElement(SaveBtn_Programs).Click();

        }
        internal void Programs_Sessional_4yo3yo_Separate()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            Thread.Sleep(3000);
            wait.Until(x => x.FindElement(Edit_Element));
            Driver.FindElement(Edit_Element).Click();
            Thread.Sleep(2000);
            wait.Until(x => x.FindElement(RotationalSessionalModel));
            Driver.FindElement(RotationalSessionalModel).Click();
            wait.Until(x => x.FindElement(RotationalModel_Deliver_No));
            Driver.FindElement(RotationalModel_Deliver_No).Click();
            wait.Until(x => x.FindElement(Fouryo3yo_Separate_Radio));
            Driver.FindElement(Fouryo3yo_Separate_Radio).Click();
            wait.Until(x => x.FindElement(ESKProgram_Yes));
            Driver.FindElement(ESKProgram_Yes).Click();
            wait.Until(x => x.FindElement(SaveBtn_Programs));
            Driver.FindElement(SaveBtn_Programs).Click();

        }
      

        internal void Programs_SessionalIntegrated_RotationalYes_4yo(string weeks)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            Thread.Sleep(3000);
            wait.Until(x => x.FindElement(Edit_Element));
            Driver.FindElement(Edit_Element).Click();
            Thread.Sleep(2000);
            wait.Until(x => x.FindElement(SessionalIntegratedModel));
            Driver.FindElement(SessionalIntegratedModel).Click();
            wait.Until(x => x.FindElement(RotationalModel_Deliver_Yes));
            Driver.FindElement(RotationalModel_Deliver_Yes).Click(); 
            Kindergarten_Weeks.Click();
            Kindergarten_Weeks.Clear();
            Kindergarten_Weeks.SendKeys(weeks);
            wait.Until(x => x.FindElement(FourYearsOnly_Radio));
            Driver.FindElement(FourYearsOnly_Radio).Click();
            wait.Until(x => x.FindElement(SaveBtn_Programs));
            Driver.FindElement(SaveBtn_Programs).Click();

        }

        internal void Programs_SessionalIntegrated_RotationalNo_4yo(string weeks)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            Thread.Sleep(3000);
            wait.Until(x => x.FindElement(Edit_Element));
            Driver.FindElement(Edit_Element).Click();
            Thread.Sleep(2000);
            wait.Until(x => x.FindElement(RotationalSessionalModel));
            Driver.FindElement(RotationalSessionalModel).Click();
            wait.Until(x => x.FindElement(RotationalModel_Deliver_No));
            Driver.FindElement(RotationalModel_Deliver_No).Click();
            Kindergarten_Weeks.Click();
            Kindergarten_Weeks.Clear();
            Kindergarten_Weeks.SendKeys(weeks);
            wait.Until(x => x.FindElement(FourYearsOnly_Radio));
            Driver.FindElement(FourYearsOnly_Radio).Click();
            wait.Until(x => x.FindElement(SaveBtn_Programs));
            Driver.FindElement(SaveBtn_Programs).Click();

        }
        internal void Programs_SessionalIntegrated_RotationalYes_4yo3yo_Separate(string weeks)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            Thread.Sleep(3000);
            wait.Until(x => x.FindElement(Edit_Element));
            Driver.FindElement(Edit_Element).Click();
            Thread.Sleep(2000);
            wait.Until(x => x.FindElement(SessionalIntegratedModel));
            Driver.FindElement(SessionalIntegratedModel).Click();
            wait.Until(x => x.FindElement(RotationalModel_Deliver_Yes));
            Driver.FindElement(RotationalModel_Deliver_Yes).Click();
            Thread.Sleep(2000);
            Kindergarten_Weeks.SendKeys(weeks);
            wait.Until(x => x.FindElement(Fouryo3yo_Separate_Radio));
            Driver.FindElement(Fouryo3yo_Separate_Radio).Click();
            wait.Until(x => x.FindElement(SaveBtn_Programs));
            Driver.FindElement(SaveBtn_Programs).Click();

        }

        internal void Programs_SessionalIntegrated_RotationalNo_4yo3yo_Separate(string weeks)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            Thread.Sleep(3000);
            wait.Until(x => x.FindElement(Edit_Element));
            Driver.FindElement(Edit_Element).Click();
            Thread.Sleep(2000);
            wait.Until(x => x.FindElement(RotationalSessionalModel));
            Driver.FindElement(RotationalSessionalModel).Click();
            wait.Until(x => x.FindElement(RotationalModel_Deliver_No));
            Driver.FindElement(RotationalModel_Deliver_No).Click();
            Thread.Sleep(2000);
            Kindergarten_Weeks.SendKeys(weeks);
            wait.Until(x => x.FindElement(Fouryo3yo_Separate_Radio));
            Driver.FindElement(Fouryo3yo_Separate_Radio).Click();
            wait.Until(x => x.FindElement(SaveBtn_Programs));
            Driver.FindElement(SaveBtn_Programs).Click();

        }

        internal void Programs_SessionalIntegrated_RotationalYes_4yo3yo_Combine(string weeks)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            Thread.Sleep(3000);
            wait.Until(x => x.FindElement(Edit_Element));
            Driver.FindElement(Edit_Element).Click();
            Thread.Sleep(2000);
            wait.Until(x => x.FindElement(SessionalIntegratedModel));
            Driver.FindElement(SessionalIntegratedModel).Click();
            wait.Until(x => x.FindElement(RotationalModel_Deliver_Yes));
            Driver.FindElement(RotationalModel_Deliver_Yes).Click();
            Thread.Sleep(2000);
            Kindergarten_Weeks.SendKeys(weeks);
            wait.Until(x => x.FindElement(Fouryo3yo_Combined_Radio));
            Driver.FindElement(Fouryo3yo_Combined_Radio).Click();
            wait.Until(x => x.FindElement(SaveBtn_Programs));
            Driver.FindElement(SaveBtn_Programs).Click();

        }

        internal void Programs_SessionalIntegrated_RotationalNo_4yo3yo_Combine(string weeks)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            Thread.Sleep(3000);
            wait.Until(x => x.FindElement(Edit_Element));
            Driver.FindElement(Edit_Element).Click();
            Thread.Sleep(2000);
            wait.Until(x => x.FindElement(RotationalSessionalModel));
            Driver.FindElement(RotationalSessionalModel).Click();
            wait.Until(x => x.FindElement(RotationalModel_Deliver_No));
            Driver.FindElement(RotationalModel_Deliver_No).Click();
            Thread.Sleep(2000);
            Kindergarten_Weeks.SendKeys(weeks);
            wait.Until(x => x.FindElement(Fouryo3yo_Combined_Radio));
            Driver.FindElement(Fouryo3yo_Combined_Radio).Click();
            wait.Until(x => x.FindElement(SaveBtn_Programs));
            Driver.FindElement(SaveBtn_Programs).Click();

        }


        internal void Programs_ExistingService()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();

            //To add the sessional group data
            wait.Until(x => x.FindElement(AddSessionalGroup_Element));
            Driver.FindElement(AddSessionalGroup_Element).Click();
        }
        internal void FillSessionalGroupForm(Table table)
        {
            //To add the sessional group data
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(AddSessionalGroup_Element));
            Thread.Sleep(2000);
            //Driver.FindElement(Program_Edit_FillDetails).Click();
            Driver.FindElement(AddSessionalGroup_Element).Click();

            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                
                GroupName_Element.SendKeys(row.ItemArray[0].ToString());

                SelectElement Start = new SelectElement(Driver.FindElement(Start_Element));
                Start.SelectByText(row.ItemArray[1].ToString());

                SelectElement End = new SelectElement(Driver.FindElement(End_Element));
                End.SelectByText(row.ItemArray[2].ToString());

                SelectElement StartSelect = new SelectElement(Driver.FindElement(By.Id("grpSessStart2")));
                StartSelect.SelectByIndex(7);

                SelectElement Finishselect = new SelectElement(Driver.FindElement(By.Id("grpSessFinish2")));
                Finishselect.SelectByIndex(41);

                Driver.FindElement(FeesCharged_Element).SendKeys(row.ItemArray[3].ToString());

                SelectElement FeePeriod = new SelectElement(Driver.FindElement(FeePeriod_Element));
                FeePeriod.SelectByText(row.ItemArray[4].ToString());

                //code for selecting Teachers from listbox

                IWebElement LI = Driver.FindElement(Listbox_Element);
                LI.FindElement(LB_Select).Click();
                wait.Until(x => x.FindElement(SaveBtn_SessionalGroup));
                Driver.FindElement(SaveBtn_SessionalGroup).Click();
                Thread.Sleep(2000);
                //Add sessional group button if there is multiple data to fill
                //wait.Until(x => x.FindElement(AddSessionalGroup_Element));
                //Driver.FindElement(AddSessionalGroup_Element).Click();
            }
        }

        internal void SubmitProcessing()
        {
            Thread.Sleep(2000);
            if (Driver.FindElement(ExitBtn_SessionalGroup).Displayed)
            {
                Driver.FindElement(ExitBtn_SessionalGroup).Click();

                try
                {
                    // Check the presence of alert
                    IAlert alert = Driver.SwitchTo().Alert();
                    //Click ok on the alert
                    alert.Accept();

                }
                catch (NoAlertPresentException)
                { }
            }
            Thread.Sleep(2000);
            Driver.FindElement(SubmitProcessing_Button).Click();

        }

        internal void DeleteProgram(string groupName)
        {

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            wait.Until(x => x.FindElement(SearchPrograms_Element));
            Driver.FindElement(SearchPrograms_Element).SendKeys(groupName);
            //Click on remove link
            wait.Until(x => x.FindElement(RemovePrograms_Link));
            Driver.FindElement(RemovePrograms_Link).Click();
        }

        internal void Programs_Sessional_Rotational_4yo()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            Thread.Sleep(3000);
            wait.Until(x => x.FindElement(Edit_Element));
            Driver.FindElement(Edit_Element).Click();
            wait.Until(x => x.FindElement(RotationalSessionalModel));
            Driver.FindElement(RotationalSessionalModel).Click();
            wait.Until(x => x.FindElement(RotationalModel_Deliver_Yes));
            Driver.FindElement(RotationalModel_Deliver_Yes).Click();
            wait.Until(x => x.FindElement(FourYearsOnly_Radio));
            Driver.FindElement(FourYearsOnly_Radio).Click();
            wait.Until(x => x.FindElement(SaveBtn_Programs));
            Driver.FindElement(SaveBtn_Programs).Click();
        }

        internal void Programs_Sessional_Rotational_4yo3yo_Separate()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            Thread.Sleep(3000);
            wait.Until(x => x.FindElement(Edit_Element));
            Driver.FindElement(Edit_Element).Click();
            Thread.Sleep(2000);
            wait.Until(x => x.FindElement(RotationalSessionalModel));
            Driver.FindElement(RotationalSessionalModel).Click();
            wait.Until(x => x.FindElement(RotationalModel_Deliver_Yes));
            Driver.FindElement(RotationalModel_Deliver_Yes).Click();
            wait.Until(x => x.FindElement(Fouryo3yo_Separate_Radio));
            Driver.FindElement(Fouryo3yo_Separate_Radio).Click();
            wait.Until(x => x.FindElement(ESKProgram_Yes));
            Driver.FindElement(ESKProgram_Yes).Click();
            wait.Until(x => x.FindElement(SaveBtn_Programs));
            Driver.FindElement(SaveBtn_Programs).Click();
        }

        internal void FillSessionalGroupForm_NewService(Table table)
        {
            //To add the sessional group data for new service
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            //wait.Until(x => x.FindElement(AddSessionalGroup_Element));
            Thread.Sleep(2000);
            Driver.FindElement(Program_Edit_FillDetails).Click();
            //Driver.FindElement(AddSessionalGroup_Element).Click();

            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {

                GroupName_Element.SendKeys(row.ItemArray[0].ToString());

                SelectElement Start = new SelectElement(Driver.FindElement(Start_Element));
                Start.SelectByText(row.ItemArray[1].ToString());

                SelectElement End = new SelectElement(Driver.FindElement(End_Element));
                End.SelectByText(row.ItemArray[2].ToString());

                SelectElement StartSelect = new SelectElement(Driver.FindElement(By.Id("grpSessStart2")));
                StartSelect.SelectByIndex(7);

                SelectElement Finishselect = new SelectElement(Driver.FindElement(By.Id("grpSessFinish2")));
                Finishselect.SelectByIndex(41);

                Driver.FindElement(FeesCharged_Element).SendKeys(row.ItemArray[3].ToString());

                SelectElement FeePeriod = new SelectElement(Driver.FindElement(FeePeriod_Element));
                FeePeriod.SelectByText(row.ItemArray[4].ToString());

                //code for selecting Teachers from listbox

                IWebElement LI = Driver.FindElement(Listbox_Element);
                LI.FindElement(LB_Select).Click();
                wait.Until(x => x.FindElement(SaveBtn_SessionalGroup));
                Driver.FindElement(SaveBtn_SessionalGroup).Click();
                Thread.Sleep(2000);
                //Add sessional group button if there is multiple data to fill
                //wait.Until(x => x.FindElement(AddSessionalGroup_Element));
                //Driver.FindElement(AddSessionalGroup_Element).Click();
            }
        }

            internal void Programs_Integrated_Without3yo(string weeks)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            Thread.Sleep(2000);
            wait.Until(x => x.FindElement(Edit_Element));
            Driver.FindElement(Edit_Element).Click();
            Thread.Sleep(2000);
            wait.Until(x => x.FindElement(IntegratedModel));
            Driver.FindElement(IntegratedModel).Click();
            Kindergarten_Weeks.Click();
            Kindergarten_Weeks.Clear();
            Kindergarten_Weeks.SendKeys(weeks);
            wait.Until(x => x.FindElement(ESKProgram_Integrated_No));
            Driver.FindElement(ESKProgram_Integrated_No).Click();
            wait.Until(x => x.FindElement(SaveBtn_Programs));
            Driver.FindElement(SaveBtn_Programs).Click();
        }
        public IWebElement Edit_Integrated => Driver.FindElement(By.XPath("//tbody[@id='ProgramSummaryDataGrid']//button[@class='btn blue edit'][contains(text(),'Edit')]"));
        public By TeacherName => By.XPath("//div[@id='programAddEditModal']//div[@class='modal-dialog']//select[@id='proTeacherName']");
        public By MondayStart => By.XPath("//div[@id='programAddEditModal']//div[@class='modal-dialog']//select[@id='teacherHrsStart1']");
        public By MondayEnd => By.XPath("//div[@id='programAddEditModal']//div[@class='modal-dialog']//select[@id='teacherHrsFinish1']");
        public By WednesdayStart => By.XPath("//div[@id='programAddEditModal']//div[@class='modal-dialog']//select[@id='teacherHrsStart13']");
        public By WednesdayEnd => By.XPath("//div[@id='programAddEditModal']//div[@class='modal-dialog']//select[@id='teacherHrsFinish3']");
        public By TuesdayStart => By.XPath("//div[@id='programAddEditModal']//div[@class='modal-dialog']//select[@id='teacherHrsStart2']");
        public By TuesdayEnd => By.XPath("//div[@id='programAddEditModal']//div[@class='modal-dialog']//select[@id='teacherHrsFinish2']");
        public By ThursdayStart => By.XPath("//div[@id='programAddEditModal']//div[@class='modal-dialog']//select[@id='teacherHrsStart4']");
        public By ThursdayEnd => By.XPath("//div[@id='programAddEditModal']//div[@class='modal-dialog']//select[@id='teacherHrsFinish4']");
        public By FridayStart => By.XPath("//div[@id='programAddEditModal']//div[@class='modal-dialog']//select[@id='teacherHrsStart5']");
        public By FridayEnd => By.XPath("//div[@id='programAddEditModal']//div[@class='modal-dialog']//select[@id='teacherHrsFinish5']");
        public IWebElement SaveTeacher_Integrated => Driver.FindElement(By.XPath("//form[@id='programAddEditForm']//input[@name='btnSave']")); 
        internal void FillIntegratedGroupForm(Table table)
        {
            //Fill integrated programs form
            //Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                Thread.Sleep(3000);
                //click on edit integrated program link
                Edit_Integrated.Click();
                Thread.Sleep(2000);
                SelectElement TeacherName_Select = new SelectElement(Driver.FindElement(TeacherName));
                TeacherName_Select.SelectByText(row.ItemArray[0].ToString());

                if (row.ItemArray[3].ToString()=="Monday")
                {
                    SelectElement MondayStart_Select = new SelectElement(Driver.FindElement(MondayStart));
                    MondayStart_Select.SelectByText(row.ItemArray[1].ToString());
                    SelectElement MondayEnd_Select = new SelectElement(Driver.FindElement(MondayEnd));
                    MondayEnd_Select.SelectByText(row.ItemArray[2].ToString());

                }
                if (row.ItemArray[3].ToString() == "Tuesday")
                {
                    SelectElement TuesdayStart_Select = new SelectElement(Driver.FindElement(TuesdayStart));
                    TuesdayStart_Select.SelectByText(row.ItemArray[1].ToString());
                    SelectElement TuesdayEnd_Select = new SelectElement(Driver.FindElement(TuesdayEnd));
                    TuesdayEnd_Select.SelectByText(row.ItemArray[2].ToString());
                }
               
                if (row.ItemArray[3].ToString() == "Wednesday")
                {
                    SelectElement WednesdayStart_Select = new SelectElement(Driver.FindElement(WednesdayStart));
                    WednesdayStart_Select.SelectByText(row.ItemArray[1].ToString());
                    SelectElement WednesdayEnd_Select = new SelectElement(Driver.FindElement(WednesdayEnd));
                    WednesdayEnd_Select.SelectByText(row.ItemArray[2].ToString());
                }
                if (row.ItemArray[3].ToString() == "Thurday")
                {
                    SelectElement ThursdayStart_Select = new SelectElement(Driver.FindElement(ThursdayStart));
                    ThursdayStart_Select.SelectByText(row.ItemArray[1].ToString());
                    SelectElement MondayEnd_Select = new SelectElement(Driver.FindElement(ThursdayEnd));
                    MondayEnd_Select.SelectByText(row.ItemArray[2].ToString());
                }
                if (row.ItemArray[3].ToString() == "Friday")
                {
                    SelectElement FridayStart_Select = new SelectElement(Driver.FindElement(FridayStart));
                    FridayStart_Select.SelectByText(row.ItemArray[1].ToString());
                    SelectElement FridayEnd_Select = new SelectElement(Driver.FindElement(FridayEnd));
                    FridayEnd_Select.SelectByText(row.ItemArray[2].ToString());
                }
                SaveTeacher_Integrated.Click();

            }
    }

        internal void Programs_Integrated_With3yo(string weeks)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            Thread.Sleep(2000);
            wait.Until(x => x.FindElement(Edit_Element));
            Driver.FindElement(Edit_Element).Click();
            Thread.Sleep(2000);
            wait.Until(x => x.FindElement(IntegratedModel));
            Driver.FindElement(IntegratedModel).Click();
            Kindergarten_Weeks.Click();
            Kindergarten_Weeks.Clear();
            Kindergarten_Weeks.SendKeys(weeks);
            wait.Until(x => x.FindElement(ESKProgram_Integrated_Yes));
            Driver.FindElement(ESKProgram_Integrated_Yes).Click();
            wait.Until(x => x.FindElement(SaveBtn_Programs));
            Driver.FindElement(SaveBtn_Programs).Click();
        }

        internal void Programs_Sessional_Rotational_4yo3yo_Combined()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            Thread.Sleep(3000);
            wait.Until(x => x.FindElement(Edit_Element));
            Driver.FindElement(Edit_Element).Click();
            Thread.Sleep(2000);
            wait.Until(x => x.FindElement(RotationalSessionalModel));
            Driver.FindElement(RotationalSessionalModel).Click();
            wait.Until(x => x.FindElement(RotationalModel_Deliver_Yes));
            Driver.FindElement(RotationalModel_Deliver_Yes).Click();
            wait.Until(x => x.FindElement(Fouryo3yo_Combined_Radio));
            Driver.FindElement(Fouryo3yo_Combined_Radio).Click();
            wait.Until(x => x.FindElement(ESKProgram_Yes));
            Driver.FindElement(ESKProgram_Yes).Click();
            wait.Until(x => x.FindElement(SaveBtn_Programs));
            Driver.FindElement(SaveBtn_Programs).Click();
        }

        public By Program_RemoveButton => By.XPath("//form[@id='programRemoveForm']//input[@class='btn blue btn-primary delete submit']");

        internal void FillSessionalGroupForm_Rotational(Table table, string room)
        {
            //To add sessional group for rotational model
            Thread.Sleep(2000);
            Driver.FindElement(AddSessionalGroup_Element).Click();

            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
                GroupName_Element.SendKeys(row.ItemArray[0].ToString());

                //To add rooms
                AddRoom.Click();
                RoomName_Element.SendKeys(room);
                Ok_Link.Click();

                SelectElement Start = new SelectElement(Driver.FindElement(Start_Element));
                Start.SelectByText(row.ItemArray[1].ToString());

                SelectElement End = new SelectElement(Driver.FindElement(End_Element));
                End.SelectByText(row.ItemArray[2].ToString());

                SelectElement Room1 = new SelectElement(Driver.FindElement(Room1_Dropdown));
                Room1.SelectByText(room);

                SelectElement StartSelect = new SelectElement(Driver.FindElement(By.Id("grpSessStart2")));
                StartSelect.SelectByText(row.ItemArray[1].ToString());

                SelectElement Finishselect = new SelectElement(Driver.FindElement(By.Id("grpSessFinish2")));
                Finishselect.SelectByText(row.ItemArray[2].ToString());

                //Selecting room from dropdown
                SelectElement Room2 = new SelectElement(Driver.FindElement(Room2_Dropdown));
                Room2.SelectByText(room);

                Driver.FindElement(FeesCharged_Element).SendKeys(row.ItemArray[3].ToString());

                SelectElement FeePeriod = new SelectElement(Driver.FindElement(FeePeriod_Element));
                FeePeriod.SelectByText(row.ItemArray[4].ToString());

                //code for selecting one teacher from listbox
                IWebElement LI = Driver.FindElement(Listbox_Element);
                LI.FindElement(LB_Select).Click();

                Driver.FindElement(SaveBtn_SessionalGroup).Click();
                Thread.Sleep(2000);

                //Add sessional group button if there is multiple data to fill
                Driver.FindElement(AddSessionalGroup_Element).Click();
            }
        }

        public By Element => By.XPath("//td[contains(text(),'No matching records found')]");
        public By EditPrograms_Link => By.XPath("//tbody[@id='ProgramSummaryDataGrid']//button[@class='btn blue edit'][contains(text(),'Edit')]");
        public By LinkEnrolmentsPrograms_Link => By.XPath("//tbody[@id='ProgramSummaryDataGrid']//button[@class='btn blue link enabled'][contains(text(),'Link Enrolments')]");
        public By ESKProgram_Yes => By.XPath("//form[@id='programSetUpForm']//input[@id='planned3YrOldYes']");
        public By Program_Edit_FillDetails => By.XPath("//tbody[@id='ProgramSummaryDataGrid']//button[@class='btn blue edit'][contains(text(),'Edit')]");

        public By Room1_Dropdown => By.XPath("//select[@id='grpSessMonRoom']");
        public By Room2_Dropdown => By.XPath("//select[@id='grpSessTueRoom']");

        internal void Confirm_DeleteProgram()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Program_RemoveButton));
            Driver.FindElement(Program_RemoveButton).Click();
        }

        internal void Verify_RemoveProgram(string groupName)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(SearchPrograms_Element));
            Driver.FindElement(SearchPrograms_Element).SendKeys(groupName);
            bool a = CommonFunctions.IsElementDisplayed(Driver, Element);
            string b = a.ToString();
            Assert.AreEqual(b, "True");
        }
        internal void EditProgram(string groupName)
        {

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            wait.Until(x => x.FindElement(SearchPrograms_Element));
            Driver.FindElement(SearchPrograms_Element).SendKeys(groupName);
            //Click on remove link
            wait.Until(x => x.FindElement(EditPrograms_Link));
            Driver.FindElement(EditPrograms_Link).Click();

        }

        internal void EditFees(string fees)
        {
            //Updating the fees of the program
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(FeesCharged_Element));
            //Clear the field
            //Thread.Sleep(2000);
            Driver.FindElement(FeesCharged_Element).Clear();
            Driver.FindElement(FeesCharged_Element).SendKeys(fees);
            wait.Until(x => x.FindElement(SaveBtn_SessionalGroup));
            Driver.FindElement(SaveBtn_SessionalGroup).Click();

        }

        internal void Verify_EditProgram(string groupName)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(SearchPrograms_Element));
            Driver.FindElement(SearchPrograms_Element).SendKeys(groupName);
            bool a = CommonFunctions.IsElementDisplayed(Driver, Edit_Element);
            string b = a.ToString();
            Assert.AreEqual(b, "True");
        }

        internal void CompleteProgramData()
        {

            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            Driver.FindElement(Programs_Element).Click();
            //number of rows in educator table 
            IList<IWebElement> TableRows = Driver.FindElements(By.XPath("//tbody[@id='ProgramSummaryDataGrid']//tr"));
            for (int i = 1; i <= TableRows.Count; i++)
            {
                Driver.FindElement(By.XPath("//tbody[@id='ProgramSummaryDataGrid']//tr[" + i + "]//button[contains(@class,'btn blue edit')][contains(text(),'Edit')]")).Click();
                //code for selecting Teachers from listbox
                IList<IWebElement> LI = Driver.FindElements(By.XPath("//li[contains(@id,'selectable')]"));
                for (int j = 1; j <= LI.Count; j++)
                {
                    ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].click()", Driver.FindElement(By.XPath("//li[contains(@id,'selectable')][" + j + "]")));
                    //Driver.FindElement(By.XPath("//li[contains(@id,'selectable')][" + j + "]")).Click();
                    //Driver.FindElement(Listbox_Element).Click();
                }
                Thread.Sleep(2000);
                Driver.FindElement(SaveBtn_SessionalGroup).Click();
                Thread.Sleep(2000);

            }
            if (Driver.FindElement(SubmitProcessing_Button).Enabled)
            {
                Driver.FindElement(SubmitProcessing_Button).Click();
            }

        }
        internal void LinkEnrolments_Program(string groupName)
        {

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(Programs_Element));
            Driver.FindElement(Programs_Element).Click();
            wait.Until(x => x.FindElement(SearchPrograms_Element));
            Driver.FindElement(SearchPrograms_Element).SendKeys(groupName);
            
            //Click on Link Enrolments
            wait.Until(x => x.FindElement(LinkEnrolmentsPrograms_Link));
            Driver.FindElement(LinkEnrolmentsPrograms_Link).Click();

            //code for selecting Enrolment from listbox            
            wait.Until(x => x.FindElement(LinkEnrolmentForm));

            IList<IWebElement> LI = Driver.FindElements(By.XPath("//li[@id='1592858269-selectable']"));
            for (int j = 1; j <= LI.Count; j++)
            {
                ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].click()", Driver.FindElement(By.XPath("//li[@id='1592858269-selectable'][" + j + "]")));
                //Driver.FindElement(By.XPath("//li[contains(@id,'selectable')][" + j + "]")).Click();
                //Driver.FindElement(Listbox_Element).Click();
            }
            

            //Thread.Sleep(2000);
            wait.Until(x => x.FindElement(LinkBtn_LinkEnrolment));
            Driver.FindElement(LinkBtn_LinkEnrolment).Click();
        }        
    }
}