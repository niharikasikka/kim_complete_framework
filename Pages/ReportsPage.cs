﻿using KIM_SmokeTest.BaseClass;
using OpenQA.Selenium;
using System;
using System.Threading.Tasks;
using System.Threading;
using KIM_SmokeTest.UtilityClasses;
using OpenQA.Selenium.Support.UI;
using System.Data;
using System.IO;
using NUnit.Framework;

namespace KIM_Automation.Pages
{
    class ReportsPage : BaseApplicationPage
    {
        public ReportsPage(IWebDriver driver) : base(driver)
        { }
        public IWebElement ACRReport_Link => Driver.FindElement(By.XPath("//a[contains(text(),'Annual Confirmation Report')]"));
        public IWebElement ESKSurvey_Link => Driver.FindElement(By.XPath("//a[contains(text(),'ESK Survey Report')]"));
        public IWebElement ESK_ExportToExcel_Link => Driver.FindElement(By.XPath("//input[@id='ctl00_PlaceHolderMain_btnESKReport']"));
        public IWebElement GeoCodeSummary_Link => Driver.FindElement(By.XPath("//a[contains(text(),'Geo Code Summary for Enrolments')]"));
        //public IWebElement Geo_TotalEnrolments => Driver.FindElement(By.CssSelector("#ctl00_PlaceHolderMain_txtTotalEnrolments"));
        public By Geo_TotalEnrolments => By.XPath("#ctl00_PlaceHolderMain_txtTotalEnrolments");
        public IWebElement SPGOverview_Link => Driver.FindElement(By.XPath("//a[contains(text(),'SPG Overview Report')]"));
        public IWebElement SessionalGroup_Link => Driver.FindElement(By.XPath("//a[contains(text(),'Sessional groups report for ratio funding')]"));
        public IWebElement RotationalGroup_Link => Driver.FindElement(By.XPath("//a[contains(text(),'Rotational groups extract for ratio funding')]"));
        public IWebElement NonEnglishSpeaking_Link => Driver.FindElement(By.XPath("//a[contains(text(),'Non-English Speaking Background Report')]"));
        public IWebElement NonEnglishSpeaking_ExportExcel => Driver.FindElement(By.XPath("//input[@id='ctl00_PlaceHolderMain_btnNonEnglishSpeakingReport']"));
        public IWebElement SessionalGroup_ExportExcel => Driver.FindElement(By.XPath("//input[@id='ctl00_PlaceHolderMain_btnSessionalGroupRatioFunding']"));
        public IWebElement RotationalGroup_ExportExcel_Service => Driver.FindElement(By.XPath("//span[@id='DeltaPlaceHolderMain']//a[1]"));
        public IWebElement RotationalGroup_ExportExcel_Enrolment => Driver.FindElement(By.XPath("//div[@id='s4-workspace']//a[2]"));

        public IWebElement ServiceProviderFundingCommitment_link => Driver.FindElement(By.XPath("//a[contains(text(),'Service Provider Funding Commitment Report')]"));
        public IWebElement FormsByServiceLocation_link => Driver.FindElement(By.XPath("//a[contains(text(),'Forms By Service Location')]"));
        public IWebElement ServiceSummary_link => Driver.FindElement(By.XPath("//a[contains(text(),'Service Summary Report')]"));
        public By Spinner_element => By.CssSelector("#m_sqlRsWebPart_ctl00_ReportViewer_AsyncWait_Wait");
        public By ServiceProviderFundingCommitmentReportPage_element => By.XPath("//div[contains(text(),'Service Provider Funding Commitment Report')]");
        public By FormsByServiceLocationReportPage_element => By.XPath("//span[contains(text(),'Forms By Service Location')]");
        public By ServiceSummaryReportPage_element => By.XPath("//span[contains(text(),'Service Summary Report')]");

        internal void DownloadACRReport()
        {
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(80);

            ACRReport_Link.Click();
            Task.Delay(60000).Wait();

            //verify if report is correctly downloaded            
            string DLdir = CommonFunctions.GetResourceValue("DownloadDir");
            String[] allFile = Directory.GetFiles(DLdir, "AnnualConfirmation_Report_*");
            String lastFile = allFile[allFile.Length - 1];

            if (Directory.Exists(DLdir))
            {
                if (File.Exists(lastFile))
                {
                    /*FileStream fs = new FileStream("C:\\Users\\09976245\\Documents\\Test.txt", FileMode.Create);
                    TextWriter tmp = Console.Out;
                    StreamWriter sw = new StreamWriter(fs);
                    Console.SetOut(sw);
                    Console.WriteLine("File 1 " + allFile[0]);
                    Console.WriteLine("File 1 " + lastFile);
                    Console.SetOut(tmp);
                    sw.Close();*/
                }
                else
                {
                    throw new Exception("Annual Confirmation Report doesn't exist");
                };
            }
            else
            {
                throw new Exception("Directory " + DLdir + " doesn't exist");
            }
        }

        internal void DownloadESKSurveyReport()
        {
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(80);

            ESKSurvey_Link.Click();
            ESK_ExportToExcel_Link.Click();
            Task.Delay(60000).Wait();

            //verify if report is correctly downloaded            
            string DLdir = CommonFunctions.GetResourceValue("DownloadDir");
            String[] allFile = Directory.GetFiles(DLdir, "ESKSurveyReport_*");
            String lastFile = allFile[allFile.Length - 1];

            if (Directory.Exists(DLdir))
            {
                if (File.Exists(lastFile))
                {
                    /*FileStream fs = new FileStream("C:\\Users\\09976245\\Documents\\Test.txt", FileMode.Create);
                    TextWriter tmp = Console.Out;
                    StreamWriter sw = new StreamWriter(fs);
                    Console.SetOut(sw);
                    Console.WriteLine("File 1 " + lastFile);
                    Console.SetOut(tmp);
                    sw.Close();*/
                }
                else
                {
                    throw new Exception("ESK Survey Report doesn't exist");
                };
            }
            else
            {
                throw new Exception("Directory " + DLdir + " doesn't exist");
            }
        }

        internal void DownloadSPGOverviewReport()
        {
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(80);
            IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
            js.ExecuteScript("arguments[0].click();", SPGOverview_Link);
            Task.Delay(150000).Wait();
        }

        internal void DownloadSessionalGroupReport()
        {
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(80);

            SessionalGroup_Link.Click();
            IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
            js.ExecuteScript("window.scrollBy(0,1000)");
            SessionalGroup_ExportExcel.Click();
            Task.Delay(60000).Wait();

            //verify if report is correctly downloaded            
            string DLdir = CommonFunctions.GetResourceValue("DownloadDir");
            String[] allFile = Directory.GetFiles(DLdir, "GridViewExport_*");
            String lastFile = allFile[allFile.Length - 1];
            if (Directory.Exists(DLdir))
            {
                if (File.Exists(lastFile))
                {
                    /*FileStream fs = new FileStream("C:\\Users\\09976245\\Documents\\Test.txt", FileMode.Create);
                    TextWriter tmp = Console.Out;
                    StreamWriter sw = new StreamWriter(fs);
                    Console.SetOut(sw);
                    Console.WriteLine("File 1 " + lastFile);
                    Console.SetOut(tmp);
                    sw.Close();*/
                }
                else
                {
                    throw new Exception("Sessional Group Report doesn't exist");
                };
            }
            else
            {
                throw new Exception("Directory " + DLdir + " doesn't exist");
            }
        }

        internal void DownloadRotationalGroupReport()
        {
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(80);


            RotationalGroup_Link.Click();
            RotationalGroup_ExportExcel_Service.Click();
            Driver.SwitchTo().Window(Driver.WindowHandles[0]);
            RotationalGroup_ExportExcel_Enrolment.Click();
            Task.Delay(60000).Wait();

            //verify if report is correctly downloaded            
            string DLdir = CommonFunctions.GetResourceValue("DownloadDir");
            String[] allServicesFile = Directory.GetFiles(DLdir, "rotationalservices*");
            String lastServiceFile = allServicesFile[allServicesFile.Length - 1];

            if (Directory.Exists(DLdir))
            {
                if (File.Exists(lastServiceFile))
                {
                    /*FileStream fs = new FileStream("C:\\Users\\09976245\\Documents\\Test.txt", FileMode.Create);
                    TextWriter tmp = Console.Out;
                    StreamWriter sw = new StreamWriter(fs);
                    Console.SetOut(sw);
                    Console.WriteLine("File 1 " + lastServiceFile);
                    Console.SetOut(tmp);
                    sw.Close();*/
                }
                else
                {
                    throw new Exception("Rotational Group Report (Services) doesn't exist");
                };
            }
            else
            {
                throw new Exception("Directory " + DLdir + " doesn't exist");
            }


            //String path = @"C:\Users\09976245\Downloads\";
            String[] allDetailsFile = Directory.GetFiles(DLdir, "rotationaldetails*");
            String lastDetailFile = allDetailsFile[allDetailsFile.Length - 1];

            if (Directory.Exists(DLdir))
            {
                if (File.Exists(lastDetailFile))
                {
                    /*FileStream fs = new FileStream("C:\\Users\\09976245\\Documents\\Test.txt", FileMode.Create);
                    TextWriter tmp = Console.Out;
                    StreamWriter sw = new StreamWriter(fs);
                    Console.SetOut(sw);
                    Console.WriteLine("File 1 " + lastDetailFile);
                    Console.SetOut(tmp);
                    sw.Close();*/
                }
                else
                {
                    throw new Exception("Rotational Group Report (Details) doesn't exist");
                };
            }
            else
            {
                throw new Exception("Directory " + DLdir + " doesn't exist");
            }
        }

        internal void DownloadNonEnglishSpeakingBackgroundReport()
        {
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(80);

            NonEnglishSpeaking_Link.Click();
            Task.Delay(60000).Wait();
            IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
            js.ExecuteScript("window.scrollBy(0,1000)");
            NonEnglishSpeaking_ExportExcel.Click();
            Task.Delay(60000).Wait();

            //verify if report is correctly downloaded            
            string DLdir = CommonFunctions.GetResourceValue("DownloadDir");
            String[] allFile = Directory.GetFiles(DLdir, "GridViewExport_*");
            String lastFile = allFile[allFile.Length - 1];

            if (Directory.Exists(DLdir))
            {
                if (File.Exists(lastFile))
                {
                    /*FileStream fs = new FileStream("C:\\Users\\09976245\\Documents\\Test.txt", FileMode.Create);
                    TextWriter tmp = Console.Out;
                    StreamWriter sw = new StreamWriter(fs);
                    Console.SetOut(sw);
                    Console.WriteLine("File 1 " + lastFile);
                    Console.SetOut(tmp);
                    sw.Close();*/
                }
                else
                {
                    throw new Exception("Non English Speaking Report doesn't exist");
                };
            }
            else
            {
                throw new Exception("Directory " + DLdir + " doesn't exist");
            }
        }

        internal void DownloadGeoCodeSummaryReport()
        {
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            //WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(190));
            GeoCodeSummary_Link.Click();
            //wait.Until(x => x.FindElement(Geo_TotalEnrolments));            
            Task.Delay(180000).Wait();
        }

        internal void DownloadServiceProviderFundingCommitmentReport()
        {
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            ServiceProviderFundingCommitment_link.Click();
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(190));
            wait.Until(x => x.FindElement(Spinner_element).Displayed);
            wait.Until(x => x.FindElement(ServiceProviderFundingCommitmentReportPage_element).Displayed);
        }
        internal void DownloadFormsByServiceLocationReport()
        {
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            FormsByServiceLocation_link.Click();
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(190));
            wait.Until(x => x.FindElement(FormsByServiceLocationReportPage_element).Displayed);
            //Task.Delay(180000).Wait();
        }
        internal void DownloadServiceSummaryReport()
        {
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            ServiceSummary_link.Click();
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(190));
            wait.Until(x => x.FindElement(ServiceSummaryReportPage_element).Displayed);
            //Task.Delay(180000).Wait();
        }
    }
}
